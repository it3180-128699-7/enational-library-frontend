import React from 'react';
import { useRoutes } from 'react-router-dom';

import routes from 'src/app/routes';

export default function App() {
  const routing = useRoutes(routes);

  return <>{routing}</>;
}
