export const AuthorMock = {
  id: 1,
  name: 'Thái Sẹo',
  avatar: 'https://i.imgur.com/Bxvh5sL.jpg',
  bio: 'The best author of 2021',
  createdAt: '2021-12-11T11:38:54.515Z',
  updatedAt: '2021-12-11T11:38:54.515Z',
};

export const AuthorsMock = () => {
  const AuthorList = [];

  for (let i = 0; i < 6; i++) {
    AuthorList.push({
      ...AuthorMock,
      id: i,
    });
  }
  return AuthorList;
};
