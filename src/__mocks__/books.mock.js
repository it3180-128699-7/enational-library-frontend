export const BookMock = {
  id: 1,
  sku: 'BW-10037',
  title: 'Dark multiply heart',
  image: 'https://i.imgur.com/c3ZJfZD.jpeg',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
  content:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
  pageNumber: 100,
  stock: '9999',
  isPublished: true,
  publishedDate: '2021-12-11T04:05:17.210Z',
  publisher: 'NXB RHUST',
  isExclusive: false,
  createdAt: '2021-12-11T04:05:17.210Z',
  updatedAt: '2021-12-11T04:05:17.210Z',
  category: {
    id: 1,
    title: 'Romantic',
    description: 'string',
    createdAt: '2021-12-11T04:05:17.210Z',
    updatedAt: '2021-12-11T04:05:17.210Z',
  },
  author: {
    id: 1,
    name: 'Sex Xpear',
    avatar: 'https://i.imgur.com/Bb5j7UY.jpg',
    bio: 'The best author of 2021',
    createdAt: '2021-12-11T04:05:17.210Z',
    updatedAt: '2021-12-11T04:05:17.210Z',
  },
};

export const BooksMock = (book = { title: '', image: '' }) => {
  const BookList = [];
  for (let i = 0; i < 12; i++) {
    BookList.push({
      ...BookMock,
      id: i,
      title: book.title ? book.title : BookMock.title,
      image: book.image ? book.image : BookMock.image,
    });
  }
  return BookList;
};

export const ChemistryBook = {
  image: 'https://i.imgur.com/s3FPp1o.jpg',
  title: 'Chemistry',
};

export const HomedeusBook = {
  image: 'https://i.imgur.com/xtB6vm4.jpg',
  title: 'Home De Ú',
};
