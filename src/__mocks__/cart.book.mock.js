export const CartMock = {
  id: 1,
  bookId: 1,
  bookJson: {
    author: {
      avatar: 'https://i.imgur.com/Bb5j7UY.jpg',
      bio: 'Robert Louis Stevenson',
      createdAt: '2021-12-21T03:56:33.097Z',
      id: 8,
      name: 'Mark Twain',
      updatedAt: '2021-12-21T03:56:33.097Z',
    },
    category: {
      createdAt: '2021-12-21T03:59:25.630Z',
      description: 'Adventure',
      id: 10,
      title: 'Horror',
      updatedAt: '2021-12-21T03:59:25.630Z',
    },
    content:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
    createdAt: '2021-12-21T04:00:40.826Z',
    description:
      'I could start this list with Walter Scott’s Ivanhoe or possibly Defoe’s Robinson Crusoe',
    id: 1,
    image: 'https://i.imgur.com/LYo8bih.png',
    isExclusive: false,
    isPublished: true,
    pageNumber: 100,
    publishedDate: '2021-12-21T03:53:27.000Z',
    publisher: 'NXB RHUST',
    sku: 'BW-100371',
    stock: 9996,
    title: 'The Odyssey',
    updatedAt: '2022-01-04T09:21:16.000Z',
  },
};
