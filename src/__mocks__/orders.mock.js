const orderMock = {
  id: 1,
  name: 'Ricardo Milos',
  phone: '0949494949',
  address: "Summoner's Rift",
  createdAt: '2022-01-04T01:45:34.649Z',
  updatedAt: '2022-01-04T01:45:34.649Z',
  user: {
    id: 1,
    firstName: 'John',
    lastName: 'Doe',
    email: 'user@example.com',
    phone: '0949494949',
    avatar: 'https://i.imgur.com/Bb5j7UY.jpg',
    role: 'Member',
    createdAt: '2022-01-04T01:45:34.649Z',
    updatedAt: '2022-01-04T01:45:34.649Z',
    account: {
      id: 1,
      isVip: false,
      balance: 10000,
      expirationTime: '2022-01-04T01:45:34.649Z',
      createdAt: '2022-01-04T01:45:34.649Z',
      updatedAt: '2022-01-04T01:45:34.649Z',
    },
  },
  orderBooks: [
    {
      status: 'pending',
      rentedTime: '2022-01-04T01:45:34.649Z',
      receivedTime: '2022-01-04T01:45:34.649Z',
      expirationTime: '2022-01-04T01:45:34.649Z',
      returnedTime: '2022-01-04T01:45:34.649Z',
      book: {
        id: 1,
        sku: 'BW-10037',
        title: 'Dark multiply heart',
        image: 'https://i.imgur.com/LYo8bih.png',
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
        content:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
        pageNumber: 100,
        isPublished: true,
        publishedDate: '2022-01-04T01:45:34.649Z',
        publisher: 'NXB RHUST',
        isExclusive: false,
        createdAt: '2022-01-04T01:45:34.649Z',
        updatedAt: '2022-01-04T01:45:34.649Z',
      },
    },
  ],
};
const ordersMock = [];
for (let i = 1; i <= 20; i++) {
  ordersMock.push({ ...orderMock, id: i });
}
export { orderMock, ordersMock };
