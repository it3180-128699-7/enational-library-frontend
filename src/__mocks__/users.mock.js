const userMock = {
  id: 1,
  firstName: 'John',
  lastName: 'Doe',
  email: 'user@example.com',
  phone: '0949494949',
  avatar: 'https://i.imgur.com/Bb5j7UY.jpg',
  role: 'Member',
  createdAt: '2022-01-03T03:28:31.454Z',
  updatedAt: '2022-01-03T03:28:31.455Z',
  account: {
    id: 1,
    isVip: false,
    balance: 10000,
    expirationTime: '2022-01-03T03:28:31.455Z',
    createdAt: '2022-01-03T03:28:31.455Z',
    updatedAt: '2022-01-03T03:28:31.455Z',
  },
};

const usersMock = [];
for (let i = 1; i <= 26; i++) {
  usersMock.push({ ...userMock, id: i, account: { ...userMock.account, id: i } });
}
export { userMock, usersMock };
