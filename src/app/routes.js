import MainLayout from 'src/layout/Main';
import HomePage from 'src/pages/Home';
import CategoryPage from 'src/pages/Category';
import LoginPage from 'src/pages/Auth/LoginPage';
import RegisterPage from 'src/pages/Auth/RegisterPage';
import BookPage from 'src/pages/Book';
import NotFoundPage from 'src/pages/Error/NotFoundPage';
import Cart from 'src/pages/Cart';
import PaymentPage from 'src/pages/Payment';
import AdminDashboardPage from 'src/pages/Admin/Dashboard';
import AdminUserPage from 'src/pages/Admin/User';
import AdminBookPage from 'src/pages/Admin/Book';
import AdminCategoryPage from 'src/pages/Admin/Category';
import AdminOrderPage from 'src/pages/Admin/Order';
import AdminSettingPage from 'src/pages/Admin/Setting';
import AdminAuthorPage from 'src/pages/Admin/Author';
import AuthorsPage from 'src/pages/Author';
import AuthorDetailPage from 'src/pages/Author/AuthorDetail';
import ProfilePage from 'src/pages/Profile';
import ProfileInformation from 'src/components/Profile/ProfileInformation';
import ProfileSecurity from 'src/components/Profile/ProfileSecurity';
import OrderHistory from 'src/components/Profile/OrderHistory';
import OrderDetail from 'src/components/Profile/OrderDetail';
import ProfileRenewal from 'src/components/Profile/Renewal';
import BookDetailPage from 'src/pages/BookDetail';

const routes = [
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'books', element: <BookPage /> },
      { path: 'category', element: <CategoryPage /> },
      { path: 'profile', element: <ProfilePage /> },
      { path: '/', element: <HomePage /> },
      { path: 'cart', element: <Cart /> },
      { path: 'payment', element: <PaymentPage /> },
      { path: '404', element: <NotFoundPage /> },
      { path: '/', element: <HomePage /> },
      { path: '*', element: <NotFoundPage /> },
    ],
  },
  {
    path: '/auth',
    element: <MainLayout />,
    children: [
      { path: 'login', element: <LoginPage /> },
      { path: 'register', element: <RegisterPage /> },
      { path: '*', element: <NotFoundPage /> },
    ],
  },
  {
    path: '/authors',
    element: <MainLayout />,
    children: [
      { path: ':id', element: <AuthorDetailPage /> },
      { path: '', element: <AuthorsPage /> },
      { path: '*', element: <NotFoundPage /> },
    ],
  },
  {
    path: '/admin',
    element: <MainLayout />,
    children: [
      { path: 'dashboard', element: <AdminDashboardPage /> },
      { path: 'users', element: <AdminUserPage /> },
      { path: 'books', element: <AdminBookPage /> },
      { path: 'authors', element: <AdminAuthorPage /> },
      { path: 'categories', element: <AdminCategoryPage /> },
      { path: 'orders', element: <AdminOrderPage /> },
      { path: 'settings', element: <AdminSettingPage /> },
      { path: '', element: <AdminDashboardPage /> },
      { path: '*', element: <NotFoundPage /> },
    ],
  },
  {
    path: '/book',
    element: <MainLayout />,
    children: [
      { path: ':id', element: <BookDetailPage /> },
      { path: '', element: <NotFoundPage /> },
      { path: '*', element: <NotFoundPage /> },
    ],
  },
  {
    path: '/profile',
    element: <MainLayout />,
    children: [
      { path: 'information', element: <ProfileInformation /> },
      { path: 'security', element: <ProfileSecurity /> },
      { path: 'order-history', element: <OrderHistory /> },
      { path: 'renewal', element: <ProfileRenewal /> },
      { path: '', element: <ProfileInformation /> },
      { path: '*', element: <NotFoundPage /> },
    ],
  },
  {
    path: '/profile/order-history',
    element: <MainLayout />,
    children: [
      { path: ':id', element: <OrderDetail /> },
      { path: '', element: <OrderHistory /> },
    ],
  },
];

export default routes;
