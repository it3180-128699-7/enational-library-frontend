import { configureStore } from '@reduxjs/toolkit';

import bookReducer from 'src/features/book/bookSlice';
import cartReducer from 'src/features/cart/cartSlice';
import categoryReducer from 'src/features/category/categorySlice';
import authorReducer from 'src/features/author/authorSlice';
import feedbackReducer from 'src/features/feedback/feedBackSlice';
import authenReducer from 'src/features/authentication/authenSlice';
import userOrderReducer from 'src/features/userOrder/userOrderSlice';

export const store = configureStore({
  reducer: {
    book: bookReducer,
    cart: cartReducer,
    category: categoryReducer,
    author: authorReducer,
    auth: authenReducer,
    feedback: feedbackReducer,
    order: userOrderReducer,
  },
});
