import React, { useState, useEffect, useMemo } from 'react';
import { Modal, Button, Container, Form, FormControl, Row, Col } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

import { ADMIN_ACTION } from 'src/configs/constant';
import { getToken } from 'src/utils/tokenUtil';
import authorApi from 'src/features/author/authorApi';
import imageApi from 'src/features/image/imageApi';

const schema = yup
  .object({
    name: yup.string().required('Name is required'),
    bio: yup.string().required('Biography is required'),
    avatar: yup.string().url('Invalid image url').required('Avatar is required'),
  })
  .required();

function AuthorForm({ show, setShow, author, action, addAuthor, updateAuthors }) {
  const defaultValues = useMemo(
    () => ({
      name: author?.name || '',
      avatar: author?.avatar || '',
      bio: author?.bio || '',
    }),
    [author]
  );
  const isView = action === ADMIN_ACTION.VIEW;
  const [imageInput, setImageInput] = useState(null);

  const closeForm = () => setShow(false);

  const {
    register,
    formState: { errors },
    handleSubmit,
    setValue,
    reset,
    watch,
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'onBlur',
    defaultValues,
  });

  const onSubmit = async (data) => {
    try {
      console.log(data);
      let response;
      if (action === ADMIN_ACTION.CREATE) {
        response = await authorApi.createAuthor(data, getToken());
        addAuthor(response.data);
      } else if (action === ADMIN_ACTION.UPDATE) {
        response = await authorApi.updateAuthor({ ...author, ...data }, getToken());
        updateAuthors(response.data);
      } else {
        return;
      }
      toast.success(`${action} author successfully!`);
    } catch (err) {
      toast.error(err?.response?.data?.message || `Error: ${action} author failed!`);
    }
  };

  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  // Catch event change image input
  useEffect(() => {
    if (imageInput?.length) {
      imageApi
        .upload({ image: imageInput[0], folder: 'author' }, getToken())
        .then((response) => setValue('avatar', response.data.url, { shouldDirty: true }))
        .catch((err) => toast.error(err?.response?.data?.message || 'Upload image failed!'));
    }
  }, [imageInput, setValue]);

  return (
    <>
      <Modal size="lg" show={show} onHide={closeForm}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton>
            <Modal.Title>Author detail</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Container>
              <Row>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Name</Form.Label>
                    <FormControl
                      readOnly={isView}
                      isInvalid={!!errors.name}
                      {...register('name')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.name?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
              </Row>

              <Row>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Biography</Form.Label>
                    <FormControl
                      readOnly={isView}
                      as="textarea"
                      rows={5}
                      style={{ resize: 'none' }}
                      isInvalid={!!errors.bio}
                      {...register('bio')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.bio?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
              </Row>

              <Row>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Image</Form.Label>
                    <FormControl
                      readOnly={isView}
                      disabled={isView}
                      type="file"
                      accept="image/*"
                      onChange={(e) => setImageInput(e.target.files)}
                      isInvalid={!!errors?.avatar}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.avatar?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
                <Col>
                  <div className="d-flex justify-content-end">
                    {watch('avatar') && (
                      <img
                        className="rounded-circle"
                        width={128}
                        height={128}
                        style={{ objectFit: 'cover' }}
                        src={watch('avatar')}
                        alt="author"
                      />
                    )}
                  </div>
                </Col>
              </Row>
            </Container>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeForm}>
              Close
            </Button>
            {!isView && <Button type="submit">Save</Button>}
          </Modal.Footer>
        </form>
      </Modal>
    </>
  );
}

export default AuthorForm;
