import React, { useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { FaEye as ViewIcon, FaEdit as EditIcon, FaPlus as AddIcon } from 'react-icons/fa';

import { ADMIN_ACTION } from 'src/configs/constant';
import FloatingButton from 'src/components/Button/Floating';
import AuthorForm from './AuthorForm';

function AuthorsTable({ authors, setAuthors }) {
  const [actionOpenForm, setActionOpenForm] = useState('');
  const [showAuthorForm, setShowAuthorForm] = useState(false);
  const [curAuthor, setCurAuthor] = useState(null);

  const onClickAddButton = () => {
    setCurAuthor(null);
    setActionOpenForm(ADMIN_ACTION.CREATE);
    setShowAuthorForm(true);
  };

  const onClickEditButton = (author) => {
    setCurAuthor(author);
    setActionOpenForm(ADMIN_ACTION.UPDATE);
    setShowAuthorForm(true);
  };

  const onClickViewButton = (author) => {
    setCurAuthor(author);
    setActionOpenForm(ADMIN_ACTION.VIEW);
    setShowAuthorForm(true);
  };

  const updateAuthors = (author) => {
    setAuthors(
      authors.map((_author) => {
        if (_author.id !== author.id) {
          return _author;
        } else {
          return { ...author };
        }
      })
    );
  };

  const addAuthor = (author) => {
    setAuthors([author, ...authors]);
  };

  return (
    <>
      <AuthorForm
        show={showAuthorForm}
        setShow={setShowAuthorForm}
        author={curAuthor}
        action={actionOpenForm}
        addAuthor={addAuthor}
        updateAuthors={updateAuthors}
      />

      <FloatingButton onClick={() => onClickAddButton()}>
        <AddIcon />
      </FloatingButton>

      <Table hover responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Avatar</th>
            <th>Name</th>
            <th>Bio</th>
            <th />
          </tr>
        </thead>

        <tbody>
          {authors.map((author, key) => (
            <tr key={key}>
              <td>{author.id}</td>
              <td>
                <img
                  className="rounded-circle"
                  width={36}
                  height={36}
                  style={{ objectFit: 'cover' }}
                  src={author.avatar}
                  alt="avatar"
                />
              </td>
              <td className="fw-bold">{author.name}</td>
              <td>{author.bio}</td>
              <td>
                <div className="d-flex">
                  <Button
                    variant="outline-info"
                    className="me-1"
                    onClick={() => onClickViewButton(author)}
                  >
                    <ViewIcon />
                  </Button>
                  <Button variant="outline-warning" onClick={() => onClickEditButton(author)}>
                    <EditIcon />
                  </Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}

export default AuthorsTable;
