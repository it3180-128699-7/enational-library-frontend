import React, { useState, useEffect, useMemo } from 'react';
import { Modal, Button, Container, Form, FormControl, Row, Col } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { format as formatTime } from 'date-fns';

import { ADMIN_ACTION } from 'src/configs/constant';
import { getToken } from 'src/utils/tokenUtil';
import bookApi from 'src/features/book/bookApi';
import imageApi from 'src/features/image/imageApi';

const schema = yup
  .object({
    sku: yup.string().required('SKU is required'),
    title: yup.string().required('Title is required'),
    image: yup.string().url('Invalid image url').required('Image is required'),
    description: yup.string(),
    content: yup.string().required('Title is required'),
    category: yup
      .number('Invalid category')
      .integer('Invalid category')
      .required('Category is required'),
    author: yup.number('Invalid author').integer('Invalid author').required('Author is required'),
    pageNumber: yup
      .number()
      .integer('Invalid page number')
      .positive('Invalid page number')
      .required('Page number is required'),
    stock: yup
      .number()
      .integer('Invalid stock')
      .positive('Invalid stock')
      .required('Stock is required'),
    isPublished: yup.boolean('Invalid Published status'),
    publishedDate: yup.date('Invalid published date').required('Publish date is required'),
    publisher: yup.string().required('Publisher is required'),
    isExclusive: yup.boolean('Invalid type book').required('Type of book is required'),
  })
  .required();

function BookForm({ show, setShow, book, action, addBook, updateBooks, categories, authors }) {
  const defaultValues = useMemo(
    () => ({
      sku: book?.sku || '',
      title: book?.title || '',
      image: book?.image || '',
      description: book?.description || '',
      content: book?.content || '',
      category: book?.category?.id || null,
      author: book?.author?.id || null,
      pageNumber: book?.pageNumber || 0,
      stock: book?.stock || 0,
      isPublished: book?.isPublished || false,
      publishedDate: book?.publishedDate
        ? formatTime(new Date(book.publishedDate), 'yyyy-MM-dd')
        : '2022-01-01',
      publisher: book?.publisher || '',
      isExclusive: book?.isExclusive || false,
    }),
    [book]
  );
  const isView = action === ADMIN_ACTION.VIEW;

  const [imageInput, setImageInput] = useState(null);

  const closeForm = () => setShow(false);

  const {
    register,
    formState: { errors },
    handleSubmit,
    setValue,
    reset,
    watch,
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'onBlur',
    defaultValues,
  });

  const onSubmit = async (data) => {
    try {
      let response;
      if (action === ADMIN_ACTION.CREATE) {
        response = await bookApi.createBook(data, getToken());
        addBook(response.data);
      } else if (action === ADMIN_ACTION.UPDATE) {
        response = await bookApi.updateBook({ ...book, ...data }, getToken());
        updateBooks(response.data);
      } else {
        return;
      }
      toast.success(`${action} book successfully!`);
    } catch (err) {
      toast.error(err?.response?.data?.message || `Error: ${action} book failed!`);
    }
  };

  useEffect(() => {
    console.log('Hehe');
    reset(defaultValues);
  }, [defaultValues, reset]);

  // Catch event change image input
  useEffect(() => {
    if (imageInput?.length) {
      imageApi
        .upload({ image: imageInput[0], folder: 'book' }, getToken())
        .then((response) => setValue('image', response.data.url, { shouldDirty: true }))
        .catch((err) => toast.error(err?.response?.data?.message || 'Upload image failed!'));
    }
  }, [imageInput, setValue]);

  return (
    <>
      <Modal size="lg" show={show} onHide={closeForm}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton>
            <Modal.Title>Book detail</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Container>
              <Row>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>SKU</Form.Label>
                    <FormControl readOnly={isView} isInvalid={!!errors.sku} {...register('sku')} />
                    <FormControl.Feedback type="invalid">
                      {errors.sku?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Title</Form.Label>
                    <FormControl
                      readOnly={isView}
                      isInvalid={!!errors.title}
                      {...register('title')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.title?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Category</Form.Label>
                    <Form.Select
                      disabled={isView}
                      readOnly={isView}
                      aria-label="Category"
                      isInvalid={!!errors.category}
                      {...register('category')}
                    >
                      {categories.map((category, key) => (
                        <option key={key} value={category.id}>
                          {category.title}
                        </option>
                      ))}
                    </Form.Select>
                    <FormControl.Feedback type="invalid">
                      {errors.category?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Author</Form.Label>
                    <Form.Select
                      disabled={isView}
                      readOnly={isView}
                      aria-label="Author"
                      isInvalid={!!errors.author}
                      {...register('author')}
                    >
                      {authors.map((author, key) => (
                        <option key={key} value={author.id}>
                          {author.name}
                        </option>
                      ))}
                    </Form.Select>
                    <FormControl.Feedback type="invalid">
                      {errors.author?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
              </Row>

              <Row>
                <Col md={3}>
                  <Form.Group className="mb-3">
                    <Form.Label>Page number</Form.Label>
                    <FormControl
                      readOnly={isView}
                      type="number"
                      isInvalid={!!errors.pageNumber}
                      {...register('pageNumber')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.pageNumber?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
                <Col md={3}>
                  <Form.Group className="mb-3">
                    <Form.Label>Publisher</Form.Label>
                    <FormControl
                      readOnly={isView}
                      isInvalid={!!errors.publisher}
                      {...register('publisher')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.publisher?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
                <Col md={3}>
                  <Form.Group className="mb-3">
                    <Form.Label>Published Date</Form.Label>
                    <FormControl
                      readOnly={isView}
                      type="date"
                      isInvalid={!!errors.publishedDate}
                      {...register('publishedDate')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.publishedDate?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
                <Col md={3}>
                  <Form.Group className="mb-3">
                    <Form.Label>Published</Form.Label>
                    <Form.Select
                      disabled={isView}
                      readOnly={isView}
                      aria-label="Published"
                      isInvalid={!!errors.isPublished}
                      {...register('isPublished')}
                    >
                      {[
                        { value: true, title: '✅' },
                        { value: false, title: '❌' },
                      ].map((item, key) => (
                        <option key={key} value={item.value}>
                          {item.title}
                        </option>
                      ))}
                    </Form.Select>
                    <FormControl.Feedback type="invalid">
                      {errors.isPublished?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
              </Row>

              <Row>
                <Col md={6}>
                  <Form.Group className="mb-3">
                    <Form.Label>Description</Form.Label>
                    <FormControl
                      readOnly={isView}
                      isInvalid={!!errors.description}
                      {...register('description')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.description?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Stock</Form.Label>
                    <FormControl
                      readOnly={isView}
                      type="number"
                      isInvalid={!!errors.stock}
                      {...register('stock')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.stock?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Type</Form.Label>
                    <Form.Select
                      disabled={isView}
                      readOnly={isView}
                      aria-label="Type"
                      isInvalid={!!errors.isExclusive}
                      {...register('isExclusive')}
                    >
                      {[
                        { value: true, title: 'Exclusive' },
                        { value: false, title: 'Normal' },
                      ].map((type, key) => (
                        <option key={key} value={type.value}>
                          {type.title}
                        </option>
                      ))}
                    </Form.Select>
                    <FormControl.Feedback type="invalid">
                      {errors.isExclusive?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
              </Row>

              <Row>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Content</Form.Label>
                    <FormControl
                      readOnly={isView}
                      as="textarea"
                      rows={5}
                      style={{ resize: 'none' }}
                      isInvalid={!!errors.content}
                      {...register('content')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.content?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
              </Row>

              <Row>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Image</Form.Label>
                    <FormControl
                      readOnly={isView}
                      disabled={isView}
                      type="file"
                      accept="image/*"
                      onChange={(e) => setImageInput(e.target.files)}
                      isInvalid={!!errors?.image}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.image?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
                <Col>
                  <div className="d-flex justify-content-end">
                    {watch('image') && (
                      <img
                        width={128}
                        height={180}
                        style={{ objectFit: 'cover' }}
                        src={watch('image')}
                        alt="bookimage"
                      />
                    )}
                  </div>
                </Col>
              </Row>
            </Container>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeForm}>
              Close
            </Button>
            {!isView && <Button type="submit">Save</Button>}
          </Modal.Footer>
        </form>
      </Modal>
    </>
  );
}

export default BookForm;
