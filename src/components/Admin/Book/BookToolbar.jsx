import React, { useState } from 'react';
import { Button, Form, FormControl, Row, Col } from 'react-bootstrap';
import { FaSync as ResetIcon, FaFilter as FilterIcon } from 'react-icons/fa';

import OptionDivider from 'src/components/OptionDivider';

function BookToolbar({ filter, setFilter, categories, authors }) {
  const [curFilter, setCurFilter] = useState(filter);

  const resetFilter = () => {
    setFilter((filter) => ({
      ...filter,
      limit: 10,
      search: '',
      sort: 0,
      exclusive: 0,
      category: 0,
      author: 0,
    }));
    setCurFilter({
      ...curFilter,
      limit: 10,
      search: '',
      sort: 0,
      exclusive: 0,
      category: 0,
      author: 0,
    });
  };

  return (
    <div className="mb-3">
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          setFilter(curFilter);
        }}
      >
        <Row className="mb-3">
          <Col sm={12} md={6}>
            <FormControl
              placeholder="Search for name"
              value={curFilter.search}
              onChange={(e) => setCurFilter({ ...curFilter, search: e.target.value })}
            />
          </Col>
          <Col sm={6} md={3}>
            <Form.Select
              aria-label="Select type"
              value={curFilter.isExclusive}
              onChange={(e) => setCurFilter({ ...curFilter, exclusive: e.target.value })}
            >
              <option className="fw-bold fst-italic">Type</option>
              <OptionDivider />
              <option value={1}>Only exclusive books</option>
              <option value={2}>Only normal books</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={3}>
            <Form.Select
              aria-label="Limit"
              value={curFilter.limit}
              onChange={(e) => setCurFilter({ ...curFilter, limit: e.target.value })}
            >
              <option className="fw-bold fst-italic">Limit</option>
              <OptionDivider />
              <option value={5}>5</option>
              <option value={10}>10</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </Form.Select>
          </Col>
        </Row>

        <Row>
          <Col sm={6} md={3}>
            <Form.Select
              aria-label="Sort by"
              value={curFilter.sort}
              onChange={(e) => setCurFilter({ ...curFilter, sort: e.target.value })}
            >
              <option className="fw-bold fst-italic">Sort</option>
              <OptionDivider />
              <option value={1}>Oldest book</option>
              <option value={2}>Newest book</option>
              <option value={3}>Title ⬆️</option>
              <option value={4}>Title ⬇️</option>
              <option value={5}>Most popular</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={3}>
            <Form.Select
              aria-label="Category"
              value={curFilter.category}
              onChange={(e) => setCurFilter({ ...curFilter, category: e.target.value })}
            >
              <option className="fw-bold fst-italic">Category</option>
              <OptionDivider />
              {categories.map((category, key) => (
                <option key={key} value={category.id}>
                  {category.title}
                </option>
              ))}
            </Form.Select>
          </Col>
          <Col sm={6} md={3}>
            <Form.Select
              aria-label="Author"
              value={curFilter.author}
              onChange={(e) => setCurFilter({ ...curFilter, author: e.target.value })}
            >
              <option className="fw-bold fst-italic">Author</option>
              <OptionDivider />
              {authors.map((author, key) => (
                <option key={key} value={author.id}>
                  {author.name}
                </option>
              ))}
            </Form.Select>
          </Col>
          <Col sm={6} md={3}>
            <Row>
              <Col>
                <div className="d-grid">
                  <Button variant="secondary" onClick={resetFilter}>
                    <div className="d-flex justify-content-center align-items-center">
                      <div className="me-3">Reset</div>
                      <ResetIcon />
                    </div>
                  </Button>
                </div>
              </Col>
              <Col>
                <div className="d-grid ">
                  <Button type="submit">
                    <div className="d-flex justify-content-center align-items-center">
                      <div className="me-3">Filter</div>
                      <FilterIcon />
                    </div>
                  </Button>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default BookToolbar;
