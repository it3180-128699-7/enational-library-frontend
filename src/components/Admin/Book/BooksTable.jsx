import React, { useState } from 'react';
import { Table, DropdownButton, Dropdown, Button } from 'react-bootstrap';
import { FaEye as ViewIcon, FaEdit as EditIcon, FaPlus as AddIcon } from 'react-icons/fa';
import { toast } from 'react-toastify';
import { format as formatTime } from 'date-fns';

import { getToken } from 'src/utils/tokenUtil';
import bookApi from 'src/features/book/bookApi';
import { ADMIN_ACTION } from 'src/configs/constant';
import FloatingButton from 'src/components/Button/Floating';
import BookForm from 'src/components/Admin/Book/BookForm';

const BooksTable = ({ books, setBooks, categories, authors }) => {
  const [actionOpenForm, setActionOpenForm] = useState('');
  const [showBookForm, setShowBookForm] = useState(false);
  const [curBook, setCurBook] = useState(null);

  const handleChangeType = (bookId, isExclusive) => {
    let book = books.find((_book) => _book.id === bookId);
    book = { ...book, author: book.author.id, category: book.category.id };
    bookApi
      .updateBook({ ...book, isExclusive }, getToken())
      .then((response) => {
        toast.success('Update book successfully!');
        updateBooks(response.data);
      })
      .catch((err) => {
        toast.error(err?.response?.data?.message || 'Update book failed!');
      });
  };

  const handleChangePublished = (bookId, isPublished) => {
    let book = books.find((_book) => _book.id === bookId);
    book = { ...book, author: book.author.id, category: book.category.id };
    bookApi
      .updateBook({ ...book, isPublished }, getToken())
      .then((response) => {
        toast.success('Update book successfully!');
        updateBooks(response.data);
      })
      .catch((err) => {
        toast.error(err?.response?.data?.message || 'Update book failed!');
      });
  };

  const onClickAddButton = () => {
    setCurBook(null);
    setActionOpenForm(ADMIN_ACTION.CREATE);
    setShowBookForm(true);
  };

  const onClickEditButton = (book) => {
    setCurBook(book);
    setActionOpenForm(ADMIN_ACTION.UPDATE);
    setShowBookForm(true);
  };

  const onClickViewButton = (book) => {
    setCurBook(book);
    setActionOpenForm(ADMIN_ACTION.VIEW);
    setShowBookForm(true);
  };

  const updateBooks = (book) => {
    setBooks(
      books.map((_book) => {
        if (_book.id !== book.id) {
          return _book;
        } else {
          return { ...book };
        }
      })
    );
  };

  const addBook = (book) => {
    setBooks([book, ...books]);
  };

  return (
    <>
      <BookForm
        show={showBookForm}
        setShow={setShowBookForm}
        action={actionOpenForm}
        book={curBook}
        addBook={addBook}
        updateBooks={updateBooks}
        categories={categories}
        authors={authors}
      />

      <FloatingButton onClick={onClickAddButton}>
        <AddIcon />
      </FloatingButton>

      <Table responsive hover={true}>
        <thead>
          <tr>
            <th>#</th>
            <th>SKU</th>
            <th>Image</th>
            <th>Title</th>
            <th>Author</th>
            <th>Category</th>
            <th>Page number</th>
            <th>Type</th>
            <th>Published</th>
            <th>Stock</th>
            <th>Publisher</th>
            <th>Published on</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {books.map((book, key) => (
            <tr key={key}>
              <td>{key + 1}</td>
              <td className="fw-bold">{book.sku}</td>
              <td>
                <img
                  style={{ objectFit: 'cover' }}
                  width={64}
                  height={90}
                  src={book.image}
                  alt="bookimage"
                />
              </td>
              <td className="fw-bold">{book.title}</td>
              <td>{book.author.name}</td>
              <td>{book.category.title}</td>
              <td>{book.pageNumber}</td>

              <td>
                <DropdownButton
                  variant={book.isExclusive ? 'warning' : 'outline-secondary'}
                  id="dropdown-basic-button"
                  title={book.isExclusive ? 'Exclusive' : 'Normal'}
                >
                  <Dropdown.Item
                    active={book.isExclusive}
                    onClick={() => handleChangeType(book.id, true)}
                  >
                    Exclusive
                  </Dropdown.Item>
                  <Dropdown.Item
                    active={!book.isExclusive}
                    onClick={() => handleChangeType(book.id, false)}
                  >
                    Normal
                  </Dropdown.Item>
                </DropdownButton>
              </td>

              <td>
                <DropdownButton
                  variant="outline-secondary"
                  id="dropdown-basic-button"
                  title={book.isPublished ? '✅' : '❌'}
                >
                  <Dropdown.Item
                    active={book.isPublished}
                    onClick={() => handleChangePublished(book.id, true)}
                  >
                    ✅
                  </Dropdown.Item>
                  <Dropdown.Item
                    active={!book.isPublished}
                    onClick={() => handleChangePublished(book.id, false)}
                  >
                    ❌
                  </Dropdown.Item>
                </DropdownButton>
              </td>

              <td>{book.stock}</td>
              <td>{book.publisher}</td>
              <td>{formatTime(new Date(book.publishedDate), 'MMM, dd yyyy')}</td>

              <td>
                <div className="d-flex">
                  <Button
                    variant="outline-info"
                    className="me-1"
                    onClick={() => onClickViewButton(book)}
                  >
                    <ViewIcon />
                  </Button>
                  <Button variant="outline-warning" onClick={() => onClickEditButton(book)}>
                    <EditIcon />
                  </Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};

export default BooksTable;
