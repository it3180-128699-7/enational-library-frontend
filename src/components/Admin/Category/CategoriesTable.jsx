import React, { useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { FaEye as ViewIcon, FaEdit as EditIcon, FaPlus as AddIcon } from 'react-icons/fa';

import { ADMIN_ACTION } from 'src/configs/constant';
import FloatingButton from 'src/components/Button/Floating';
import CategoryForm from './CategoryForm';

function CategoriesTable({ categories, setCategories }) {
  const [actionOpenForm, setActionOpenForm] = useState('');
  const [showCategoryForm, setShowCategoryForm] = useState(false);
  const [curCategory, setCurCategory] = useState(null);

  const onClickAddButton = () => {
    setCurCategory(null);
    setActionOpenForm(ADMIN_ACTION.CREATE);
    setShowCategoryForm(true);
  };

  const onClickEditButton = (category) => {
    setCurCategory(category);
    setActionOpenForm(ADMIN_ACTION.UPDATE);
    setShowCategoryForm(true);
  };

  const onClickViewButton = (category) => {
    setCurCategory(category);
    setActionOpenForm(ADMIN_ACTION.VIEW);
    setShowCategoryForm(true);
  };

  const updateCategories = (category) => {
    setCategories(
      categories.map((_category) => {
        if (_category.id !== category.id) {
          return _category;
        } else {
          return { ...category };
        }
      })
    );
  };

  const addCategory = (category) => {
    setCategories([category, ...categories]);
  };

  return (
    <>
      <CategoryForm
        show={showCategoryForm}
        setShow={setShowCategoryForm}
        category={curCategory}
        action={actionOpenForm}
        addCategory={addCategory}
        updateCategories={updateCategories}
      />

      <FloatingButton onClick={() => onClickAddButton()}>
        <AddIcon />
      </FloatingButton>

      <Table hover responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
            <th />
          </tr>
        </thead>

        <tbody>
          {categories.map((category, key) => (
            <tr key={key}>
              <td>{category.id}</td>
              <td className="fw-bold">{category.title}</td>
              <td>{category.description}</td>
              <td>
                <div className="d-flex">
                  <Button
                    variant="outline-info"
                    className="me-1"
                    onClick={() => onClickViewButton(category)}
                  >
                    <ViewIcon />
                  </Button>
                  <Button variant="outline-warning" onClick={() => onClickEditButton(category)}>
                    <EditIcon />
                  </Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}

export default CategoriesTable;
