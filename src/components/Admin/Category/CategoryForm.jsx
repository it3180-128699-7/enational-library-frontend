import React, { useEffect, useMemo } from 'react';
import { Modal, Button, Container, Form, FormControl, Row, Col } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

import { ADMIN_ACTION } from 'src/configs/constant';
import { getToken } from 'src/utils/tokenUtil';
import categoryApi from 'src/features/category/categoryApi';

const schema = yup
  .object({
    title: yup.string().required('Title is required'),
    description: yup.string(),
  })
  .required();

function CategoryForm({ show, setShow, category, action, addCategory, updateCategories }) {
  const defaultValues = useMemo(
    () => ({
      title: category?.title || '',
      description: category?.description || '',
    }),
    [category]
  );
  const isView = action === ADMIN_ACTION.VIEW;

  const closeForm = () => setShow(false);

  const {
    register,
    formState: { errors },
    handleSubmit,
    reset,
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'onBlur',
    defaultValues,
  });

  const onSubmit = async (data) => {
    try {
      console.log(data);
      let response;
      if (action === ADMIN_ACTION.CREATE) {
        response = await categoryApi.createCategory(data, getToken());
        addCategory(response.data);
      } else if (action === ADMIN_ACTION.UPDATE) {
        response = await categoryApi.updateCategory({ ...category, ...data }, getToken());
        updateCategories(response.data);
      } else {
        return;
      }
      toast.success(`${action} category successfully!`);
    } catch (err) {
      toast.error(err?.response?.data?.message || `Error: ${action} category failed!`);
    }
  };

  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <>
      <Modal size="lg" show={show} onHide={closeForm}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Modal.Header closeButton>
            <Modal.Title>Category detail</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Container>
              <Row>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Title</Form.Label>
                    <FormControl
                      required
                      readOnly={isView}
                      isInvalid={!!errors.title}
                      {...register('title')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.title?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
              </Row>

              <Row>
                <Col>
                  <Form.Group className="mb-3">
                    <Form.Label>Description</Form.Label>
                    <FormControl
                      readOnly={isView}
                      as="textarea"
                      rows={5}
                      style={{ resize: 'none' }}
                      isInvalid={!!errors.description}
                      {...register('description')}
                    />
                    <FormControl.Feedback type="invalid">
                      {errors.description?.message}
                    </FormControl.Feedback>
                  </Form.Group>
                </Col>
              </Row>
            </Container>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeForm}>
              Close
            </Button>
            {!isView && <Button type="submit">Save</Button>}
          </Modal.Footer>
        </form>
      </Modal>
    </>
  );
}

export default CategoryForm;
