import React from 'react';
import { Navbar, Container, Nav, Dropdown } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';

import { selectUser, logout } from 'src/features/authentication/authenSlice';
import './Navbar.css';

const AdminNavbar = ({ title }) => {
  const dispatch = useDispatch();
  const userData = useSelector(selectUser);

  return (
    <Navbar className="admin-navbar" bg="light" expand="lg">
      <Container fluid>
        <div className="d-flex justify-content-center align-items-center ml-2 ml-lg-0">
          <Navbar.Brand>{title}</Navbar.Brand>
        </div>
        <Navbar.Collapse className="justify-content-end" id="basic-navbar-nav">
          <Nav className="nav mr-auto" navbar>
            <Dropdown as={Nav.Item}>
              <Dropdown.Toggle
                as={Nav.Link}
                data-toggle="dropdown"
                id="dropdown-67443507"
                variant="default"
                className="m-0"
              >
                {!!userData ? userData.firstName + ' ' + userData.lastName : ''}
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item onClick={() => dispatch(logout())}>Log Out</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default AdminNavbar;
