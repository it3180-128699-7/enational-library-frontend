import React, { useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { format as formatTime } from 'date-fns';

import { getToken } from 'src/utils/tokenUtil';
import orderApi from 'src/features/admin-order/orderApi';
import { ORDER_BOOK_STATUS } from 'src/configs/constant';
import Dialog from 'src/components/Dialog';

const OrderCollapseTable = ({ order, updateOrders }) => {
  const [showDialogConfirm, setShowDialogConfirm] = useState(false);
  const [curOrderBook, setCurOrderBook] = useState(null);

  const handleCloseDialogConfirm = () => setShowDialogConfirm(false);

  const onClickConfirmButton = (orderBook) => {
    setCurOrderBook(orderBook);
    setShowDialogConfirm(true);
  };

  const confirmReturnedBook = () => {
    const bookId = curOrderBook.book.id;
    const items = [
      {
        orderId: order.id,
        bookId,
      },
    ];
    orderApi
      .confirmReturned(items, getToken())
      .then((response) => {
        toast.success('Confirm returned book successfully!');
        updateOrders({
          ...order,
          orderBooks: order.orderBooks.map((orderBook) => {
            if (orderBook.book.id === bookId) {
              return { ...orderBook, status: ORDER_BOOK_STATUS.RECEIVED };
            }
            return orderBook;
          }),
        });
      })
      .catch((err) => {
        toast.error(err?.response?.data?.message || 'Confirm returned book failed!');
      })
      .finally(() => {
        handleCloseDialogConfirm();
      });
  };

  return (
    <>
      <Dialog
        show={showDialogConfirm}
        setShow={setShowDialogConfirm}
        title="Warning"
        body="Are you sure to confirm return this book?"
        buttonTitle1="Cancel"
        buttonAction1={handleCloseDialogConfirm}
        buttonTitle2="Sure"
        buttonAction2={confirmReturnedBook}
      />

      <Table striped hover borderless size="sm" responsive>
        <thead>
          <tr>
            <th>SKU</th>
            <th>Book</th>
            <th>Type</th>
            <th>Stock</th>
            <th>Received at</th>
            <th>Expiration</th>
            <th>Returned at</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {order.orderBooks.map((orderBookItem, key) => (
            <tr key={key}>
              <td className="fw-bold">{orderBookItem.book.sku}</td>
              <td>{orderBookItem.book.title}</td>
              <td>{orderBookItem.book.isExlusive ? 'Exclusive' : 'Nomal'}</td>
              <td>{orderBookItem.book.stock}</td>
              <td>
                {orderBookItem.receivedTime
                  ? formatTime(new Date(orderBookItem.receivedTime), 'HH:mm:ss MMM, dd yyyy')
                  : '❌'}
              </td>
              <td>{formatTime(new Date(orderBookItem.expirationTime), 'HH:mm:ss MMM, dd yyyy')}</td>
              <td>
                {orderBookItem.returnedTime
                  ? formatTime(new Date(orderBookItem.expirationTime), 'HH:mm:ss MMM, dd yyyy')
                  : '❌'}
              </td>
              <td>
                <div className="d-grid fw-bold">
                  {orderBookItem.status !== ORDER_BOOK_STATUS.RETURNING ? (
                    <>
                      {orderBookItem.status}
                      {orderBookItem.status === ORDER_BOOK_STATUS.RETURNED && '✅'}
                    </>
                  ) : (
                    <div className="d-flex align-items-center">
                      <Button
                        variant="outline-success"
                        onClick={() => onClickConfirmButton(orderBookItem)}
                      >
                        {orderBookItem.status}
                      </Button>
                    </div>
                  )}
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};
export default OrderCollapseTable;
