import React, { useState } from 'react';
import { Table, Card } from 'react-bootstrap';
import { FaChevronDown as DownIcon, FaChevronUp as UpIcon } from 'react-icons/fa';
import { format as formatTime } from 'date-fns';

import './OrderTable.css';
import OrderCollapseTable from './OrderCollapseTable';

const OrdersTable = ({ orders, setOrders }) => {
  const [collapsedShowIndex, setCollapsedShowIndex] = useState(null);

  const updateOrders = (order) => {
    setOrders(
      orders.map((_order) => {
        if (_order.id !== order.id) {
          return _order;
        } else {
          return { ...order };
        }
      })
    );
  };

  return (
    <Table responsive hover>
      <thead>
        <tr>
          <th />
          <th>Order ID</th>
          <th>Receiver</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Address</th>
          <th>Created at</th>
        </tr>
      </thead>

      <tbody>
        {orders.map((order, key) => (
          <React.Fragment key={key}>
            <tr>
              <td>
                <div
                  className="collapse-button position-relative"
                  onClick={() => setCollapsedShowIndex(collapsedShowIndex === key ? null : key)}
                >
                  {order.orderBooks.some((orderBook) => orderBook.status === 'returning') && (
                    <span className="position-absolute top-0 start-100 translate-middle p-1 bg-danger border border-light rounded-circle">
                      <span className="visually-hidden">unread messages</span>
                    </span>
                  )}
                  {collapsedShowIndex === key ? <DownIcon /> : <UpIcon />}
                </div>
              </td>
              <td className="fw-bold">{`#${order.id}`}</td>
              <td>{order.name}</td>
              <td>{order.user.email}</td>
              <td>{order.user.phone}</td>
              <td>{order.address}</td>
              <td>{formatTime(new Date(order.createdAt), 'MMM, dd yyyy')}</td>
            </tr>
            <tr className={`collapse ${key === collapsedShowIndex ? 'show' : ''}`}>
              <td />
              <td colSpan={6}>
                <Card text="info">
                  <Card.Header>
                    <Card.Title>Items</Card.Title>
                  </Card.Header>
                  <Card.Body>
                    <OrderCollapseTable order={order} updateOrders={updateOrders} />
                  </Card.Body>
                </Card>
              </td>
            </tr>
          </React.Fragment>
        ))}
      </tbody>
    </Table>
  );
};

export default OrdersTable;
