import React, { useState } from 'react';
import { Button, Form, FormControl, Row, Col } from 'react-bootstrap';

import OptionDivider from 'src/components/OptionDivider';

function OrderToolBar({ filter, setFilter }) {
  const [curFilter, setCurFilter] = useState(filter);

  return (
    <div className="mb-3">
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          setFilter(curFilter);
        }}
      >
        <Row className="mb-3">
          <Col sm={12} md={4}>
            <FormControl
              placeholder="Search order by user's name"
              onChange={(e) => setCurFilter({ ...curFilter, search: e.target.value })}
            />
          </Col>
          <Col sm={6} md={2}>
            <Form.Select
              aria-label="Limit"
              value={curFilter.limit}
              onChange={(e) => setCurFilter({ ...curFilter, limit: e.target.value })}
            >
              <option className="fw-bold fst-italic">Limit</option>
              <OptionDivider />
              <option value={5}>5</option>
              <option value={10}>10</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={2}>
            <Form.Select
              aria-label="Status"
              value={curFilter.status}
              onChange={(e) => setCurFilter({ ...curFilter, status: e.target.value })}
            >
              <option className="fw-bold fst-italic">Status</option>
              <OptionDivider />
              <option value={1}>Pending orders</option>
              <option value={2}>Reiceived orders</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={2}>
            <Form.Select
              aria-label="Sort by"
              value={curFilter.sort}
              onChange={(e) => setCurFilter({ ...curFilter, sort: e.target.value })}
            >
              <option>Sort by</option>
              <option value={2}>Newest order</option>
              <option value={1}>Oldest order</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={2}>
            <div className="d-grid ">
              <Button type="submit">Search</Button>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default OrderToolBar;
