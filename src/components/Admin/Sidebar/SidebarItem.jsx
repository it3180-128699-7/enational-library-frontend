import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Nav } from 'react-bootstrap';

function SidebarItem({ title, Icon, url }) {
  const location = useLocation();

  const [active, setActive] = React.useState(false);

  React.useEffect(() => {
    if (location.pathname === url) {
      setActive(true);
    }
  }, [location.pathname, url]);

  return (
    <Nav>
      <li className="w-100 px-3 py-1">
        <Link to={url}>
          <div
            className={`p-2 fs-6 w-100 text-uppercase d-flex align-items-center justify-content-space-between sidebar-item ${
              active === true ? 'sidebar-item__active' : ''
            }`}
          >
            <Icon />
            <div className="ms-3">{title}</div>
          </div>
        </Link>
      </li>
    </Nav>
  );
}

export default SidebarItem;
