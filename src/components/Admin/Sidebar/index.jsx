import React from 'react';
import { Link } from 'react-router-dom';
import {
  FaChartLine as DashboardIcon,
  FaUsers as UsersIcon,
  FaBook as BookIcon,
  FaTag as CategoryIcon,
  FaUserEdit as AuthorIcon,
  FaShoppingBag as OrderIcon,
  FaCog as SettingIcon,
} from 'react-icons/fa';

import logo from 'src/assets/images/logo.png';
import sidebarBackground from 'src/assets/images/sidebar-background.jpg';
import './Sidebar.css';
import SidebarItem from './SidebarItem';

const items = [
  {
    title: 'Dashboard',
    Icon: DashboardIcon,
    url: '/dashboard',
  },
  {
    title: 'User',
    Icon: UsersIcon,
    url: '/users',
  },
  {
    title: 'Book',
    Icon: BookIcon,
    url: '/books',
  },
  {
    title: 'Author',
    Icon: AuthorIcon,
    url: '/authors',
  },
  {
    title: 'Category',
    Icon: CategoryIcon,
    url: '/categories',
  },
  {
    title: 'Order',
    Icon: OrderIcon,
    url: '/orders',
  },
  {
    title: 'Setting',
    Icon: SettingIcon,
    url: '/settings',
  },
];

const AdminSidebar = () => {
  return (
    <div
      className="sidebar"
      style={{
        backgroundImage: `url(${sidebarBackground})`,
      }}
    >
      <div className="sidebar-wrapper">
        <div className="sidebar-logo sidebar-logo px-3 py-4">
          <Link to="/">
            <div className="d-flex align-items-center">
              <div className="logo-img">
                <img height={30} width={30} src={logo} alt="logo" />
              </div>
              <div className="fs-5 text-uppercase sidebar-logo-text">ENational Library</div>
            </div>
          </Link>
        </div>

        {items.map((item, key) => (
          <SidebarItem key={key} title={item.title} Icon={item.Icon} url={`/admin${item.url}`} />
        ))}
      </div>
    </div>
  );
};

export default AdminSidebar;
