import React, { useState } from 'react';
import { Modal, Button, Container, Form, FormControl, Row, Col } from 'react-bootstrap';
import { format as formatTime } from 'date-fns';
import { FaPlus as AddIcon, FaTimes as XIcon } from 'react-icons/fa';
import { toast } from 'react-toastify';

import { getToken } from 'src/utils/tokenUtil';
import userApi from 'src/features/user/userApi';

function UserForm({ show, setShow, user, updateUsers }) {
  const [curDeposit, setCurDeposit] = useState(0);
  const [showDeposit, setShowDeposit] = useState(false);

  const closeForm = () => setShow(false);

  const handleSubmitForm = (e) => {
    e.preventDefault();
    if (curDeposit > 0) {
      userApi
        .depositMoney({ userId: user.id, money: curDeposit }, getToken())
        .then((response) => {
          toast.success('Deposit money successfully!');
          setCurDeposit(0);
          updateUsers(response.data);
        })
        .catch((err) => {
          toast.error(err?.response?.data?.message || 'Deposit money failed!');
        });
    }
  };

  return (
    <>
      <Modal show={show} size="lg" onHide={closeForm}>
        {user && (
          <Form onSubmit={handleSubmitForm}>
            <Modal.Header closeButton>
              <Modal.Title>User detail</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Container fluid>
                <Row>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>First name</Form.Label>
                      <FormControl readOnly value={user.firstName} />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Last name</Form.Label>
                      <FormControl readOnly value={user.lastName} />
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Email</Form.Label>
                      <FormControl readOnly value={user.email} />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Phone number</Form.Label>
                      <FormControl readOnly value={user.phone} />
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Role</Form.Label>
                      <FormControl readOnly value={user.role} />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Type</Form.Label>
                      <FormControl readOnly value={user.account.isVip ? 'Vip' : 'Normal'} />
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Balance</Form.Label>
                      <Row>
                        <Col md={10}>
                          <FormControl
                            readOnly
                            type="number"
                            value={user.account.balance + (curDeposit || 0)}
                          />
                        </Col>
                        <Col md={2}>
                          {showDeposit ? (
                            <Button variant="danger" onClick={() => setShowDeposit(false)}>
                              <XIcon />
                            </Button>
                          ) : (
                            <Button onClick={() => setShowDeposit(true)}>
                              <AddIcon />
                            </Button>
                          )}
                        </Col>
                      </Row>
                      {showDeposit && (
                        <Row>
                          <Col>
                            <Form.Group className="mb-3">
                              <Form.Label>Amount to deposit</Form.Label>
                              <FormControl
                                type="number"
                                value={curDeposit}
                                onChange={(e) => setCurDeposit(parseInt(e.target.value))}
                              />
                            </Form.Group>
                          </Col>
                        </Row>
                      )}
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Expiration</Form.Label>
                      <FormControl
                        readOnly
                        value={formatTime(
                          new Date(user.account.expirationTime),
                          'HH:mm:ss - MMM, dd yyyy'
                        )}
                      />
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Foul times</Form.Label>
                      <FormControl readOnly value={user.account.foulTimes} />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group className="mb-3">
                      <Form.Label>Joined date</Form.Label>
                      <FormControl
                        readOnly
                        value={formatTime(new Date(user.createdAt), 'HH:mm:ss - MMM, dd yyyy')}
                      />
                    </Form.Group>
                  </Col>
                </Row>
              </Container>
            </Modal.Body>

            <Modal.Footer>
              <Button variant="secondary" onClick={closeForm}>
                Cancel
              </Button>
              <Button
                type="submit"
                variant={curDeposit > 0 ? 'primary' : 'secondary'}
                disable={curDeposit <= 0}
              >
                Save
              </Button>
            </Modal.Footer>
          </Form>
        )}
      </Modal>
    </>
  );
}

export default UserForm;
