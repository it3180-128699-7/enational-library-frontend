import React, { useState } from 'react';
import { Table, DropdownButton, Dropdown, Button } from 'react-bootstrap';
import { FaEye as ViewIcon } from 'react-icons/fa';
import { toast } from 'react-toastify';

import UserForm from 'src/components/Admin/User/UserForm';
import { USER_ROLE } from 'src/configs/constant';
import userApi from 'src/features/user/userApi';
import { getToken } from 'src/utils/tokenUtil';
import { formatCurrency } from 'src/utils/common';

function UsersTable({ users, setUsers }) {
  const [selectUserId, setSelectUserId] = useState(null);
  const [showUserForm, setShowUserForm] = useState(false);

  const handleViewButton = (userId) => {
    setSelectUserId(userId);
    setShowUserForm(true);
  };

  const handleChangeStatus = (userId, status = false) => {
    userApi
      .updateStatus({ userId, isActive: status }, getToken())
      .then((response) => {
        toast.success('Change status of user successfully!');
        updateUsers(response.data);
      })
      .catch((err) => {
        toast.error(err?.response?.data?.message || 'Update role failed!');
      });
  };

  const handleChangeRole = (userId, role) => {
    userApi
      .updateRole({ userId, role }, getToken())
      .then((response) => {
        toast.success('Update role successfully!');
        updateUsers(response.data);
      })
      .catch((err) => {
        toast.error(err?.response?.data?.message || 'Update role failed!');
      })
      .finally(setSelectUserId(null));
  };

  const updateUsers = (user) => {
    const _users = users.map((_user) => {
      if (_user.id !== user.id) {
        return _user;
      } else {
        return { ...user };
      }
    });
    setUsers(_users);
  };

  return (
    <>
      <UserForm
        show={showUserForm}
        setShow={setShowUserForm}
        user={users.find((user) => user.id === selectUserId)}
        updateUsers={updateUsers}
      />
      <Table hover={true} responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Role</th>
            <th>Type</th>
            <th>Balance</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
          {users.map((user, key) => (
            <tr key={key}>
              <td>{user.id}</td>
              <td>{user.firstName}</td>
              <td>{user.lastName}</td>
              <td>{user.email}</td>
              <td>{user.phone}</td>
              <td>
                <DropdownButton
                  variant="outline-warning"
                  id="dropdown-basic-button"
                  title={user.role}
                >
                  {[
                    Object.values(USER_ROLE).map((role, key) => (
                      <Dropdown.Item
                        key={key}
                        active={role === user.role}
                        onClick={() => handleChangeRole(user.id, role)}
                      >
                        {role}
                      </Dropdown.Item>
                    )),
                  ]}
                </DropdownButton>
              </td>
              <td style={{ fontWeight: user.account.isVip ? 'bold' : 'normal' }}>
                {user.account.isVip ? 'Vip' : 'Normal'}
              </td>
              <td style={{ color: '#2ecc71', fontWeight: 'bold' }}>
                {formatCurrency(user.account.balance)}
              </td>
              <td>
                <DropdownButton
                  variant={user.account.isActive ? 'success' : 'outline-secondary'}
                  id="dropdown-basic-button"
                  title={user.account.isActive ? 'Active' : 'Banned'}
                >
                  <Dropdown.Item
                    active={user.account.isActive}
                    onClick={() => handleChangeStatus(user.id, true)}
                  >
                    Active
                  </Dropdown.Item>
                  <Dropdown.Item
                    active={!user.account.isActive}
                    onClick={() => handleChangeStatus(user.id, false)}
                  >
                    Blocked
                  </Dropdown.Item>
                </DropdownButton>
              </td>
              <td>
                <div className="d-flex">
                  <Button variant="info" onClick={() => handleViewButton(user.id)}>
                    <ViewIcon />
                  </Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}

export default UsersTable;
