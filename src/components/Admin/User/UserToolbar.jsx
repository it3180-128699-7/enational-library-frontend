import React, { useState } from 'react';
import { Button, Form, FormControl, Row, Col } from 'react-bootstrap';
import { FaSync as ResetIcon, FaFilter as FilterIcon } from 'react-icons/fa';

import OptionDivider from 'src/components/OptionDivider';

function UserToolbar({ filter, setFilter }) {
  const [curFilter, setCurFilter] = useState(filter);

  const resetFilter = () => {
    setFilter((filter) => ({
      ...filter,
      limit: 10,
      search: '',
      sort: 0,
      role: 0,
      isActive: 0,
      isVip: 0,
      isExpired: 0,
      foulTimes: 0,
    }));
    setCurFilter({
      ...curFilter,
      limit: 10,
      search: '',
      sort: 0,
      role: 0,
      isActive: 0,
      isVip: 0,
      isExpired: 0,
      foulTimes: 0,
    });
  };

  return (
    <div className="mb-3">
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          setFilter(curFilter);
        }}
      >
        <Row className="mb-3">
          <Col sm={12} md={6}>
            <FormControl
              placeholder="Search for name"
              // value={curFilter.search}
              onChange={(e) => setCurFilter({ ...curFilter, search: e.target.value })}
            />
          </Col>
          <Col sm={6} md={2}>
            <Form.Select
              aria-label="Select role"
              value={curFilter.role}
              onChange={(e) => setCurFilter({ ...curFilter, role: e.target.value })}
            >
              <option className="fw-bold fst-italic">Role</option>
              <OptionDivider />
              <option value={1}>Only admins</option>
              <option value={2}>Only staffs</option>
              <option value={3}>Only members</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={2}>
            <Form.Select
              aria-label="Limit"
              value={curFilter.limit}
              onChange={(e) => setCurFilter({ ...curFilter, limit: e.target.value })}
            >
              <option className="fw-bold fst-italic">Limit</option>
              <OptionDivider />
              <option value={5}>5</option>
              <option value={10}>10</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={2}>
            <Form.Select
              aria-label="Sort by"
              value={curFilter.sort}
              onChange={(e) => setCurFilter({ ...curFilter, sort: e.target.value })}
            >
              <option className="fw-bold fst-italic">Sort</option>
              <OptionDivider />
              <option value={1}>Oldest user</option>
              <option value={2}>Newest user</option>
              <option value={3}>First name ⬆️</option>
              <option value={4}>First name ⬇️</option>
            </Form.Select>
          </Col>
        </Row>
        <Row>
          <Col sm={6} md={2}>
            <Form.Select
              aria-label="Status"
              value={curFilter.isActive}
              onChange={(e) => setCurFilter({ ...curFilter, isActive: e.target.value })}
            >
              <option className="fw-bold fst-italic">Status</option>
              <OptionDivider />
              <option value={1}>Active accounts</option>
              <option value={2}>Banned accounts</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={2}>
            <Form.Select
              aria-label="Type"
              value={curFilter.isVip}
              onChange={(e) => setCurFilter({ ...curFilter, isVip: e.target.value })}
            >
              <option className="fw-bold fst-italic">Type</option>
              <OptionDivider />
              <option value={1}>Only vip accounts</option>
              <option value={2}>Only normal accounts</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={2}>
            <Form.Select
              aria-label="Expired"
              value={curFilter.isExpired}
              onChange={(e) => setCurFilter({ ...curFilter, isExpired: e.target.value })}
            >
              <option className="fw-bold fst-italic">Expiration</option>
              <OptionDivider />
              <option value={1}>Only expired accounts</option>
              <option value={2}>Only active accounts</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={2}>
            <Form.Select
              aria-label="Foul times"
              value={curFilter.foulTimes}
              onChange={(e) => setCurFilter({ ...curFilter, foulTimes: e.target.value })}
            >
              <option className="fw-bold fst-italic">Foul times</option>
              <OptionDivider />
              <option value={1}>Account has no foul times</option>
              <option value={2}>Account has foul times ≥ 1</option>
              <option value={3}>Account has foul times ≥ 3</option>
            </Form.Select>
          </Col>
          <Col sm={6} md={2}>
            <div className="d-grid">
              <Button variant="secondary" onClick={resetFilter}>
                <div className="d-flex justify-content-center align-items-center">
                  <div className="me-3">Reset</div>
                  <ResetIcon />
                </div>
              </Button>
            </div>
          </Col>
          <Col sm={6} md={2}>
            <div className="d-grid ">
              <Button type="submit">
                <div className="d-flex justify-content-center align-items-center">
                  <div className="me-3">Filter</div>
                  <FilterIcon />
                </div>
              </Button>
            </div>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default UserToolbar;
