import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate, Link, useLocation } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { toast } from 'react-toastify';

import { RiMailLine as MailIcon, RiLockLine as PassIcon } from 'react-icons/ri';

import { selectLoggedInState, login } from 'src/features/authentication/authenSlice';
import { authenAPI } from 'src/features/authentication/authenAPI';

import './Login.css';

const schema = yup.object().shape({
  email: yup.string().email('Please enter a valid email').required('This field is required'),
  password: yup
    .string()
    .required('This field is required')
    .min(8, 'Your password must be at least 8 characters'),
  checkbox: yup.boolean(),
});

const Login = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  //global state
  const isLoggedIn = useSelector(selectLoggedInState);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  const handleSubmitEvent = (data) => {
    authenAPI
      .loginAPI({
        email: data.email,
        password: data.password,
        remember: data.checkbox,
      })
      .then((response) => {
        toast.success('Login successful. Welcome to ENational Library.', { theme: 'colored' });
        dispatch(login(response.data));
      })
      .catch((error) => {
        toast.error('Login failed! Please check your username or password', { theme: 'colored' });
      });
  };

  if (isLoggedIn) {
    return <Navigate to={location.state?.lastUrl || '/'} />;
  }

  const submitForm = (data) => {
    handleSubmitEvent(data);
  };

  return (
    <React.Fragment>
      <div className="form-box">
        <div className="login-title">Sign In</div>
        <div className="login-title-welcome">Welcome to ENational Library</div>

        <div className="form-wrapper">
          <form className="group input-form" autoComplete="off" onSubmit={handleSubmit(submitForm)}>
            <div className="input-wrapper">
              <div className="input-wrapper-child">
                <input
                  className="input-field"
                  placeholder="Email"
                  autoComplete="off"
                  {...register('email')}
                />
                <MailIcon className="input-icon" />
              </div>
              <p className="login-page-error-message">{errors.email?.message}</p>
            </div>

            <div className="input-wrapper">
              <div className="input-wrapper-child">
                <input
                  type="password"
                  className="input-field"
                  placeholder="Password"
                  autoComplete="off"
                  {...register('password')}
                />
                <PassIcon className="input-icon" />
              </div>
              <p className="login-page-error-message">{errors.password?.message}</p>
            </div>

            <div className="checkbox-wrapper">
              <input type="checkbox" className="check-box" {...register('checkbox')} />
              <span className="checkbox-title">Remember password</span>
            </div>

            <div className="row submit-btn">
              <input className="input-submit" type="submit" />
            </div>

            <div className="login-link">
              Don't have an account ?
              <Link className="signup-link" to="/auth/register">
                Signup now
              </Link>
            </div>
          </form>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Login;
