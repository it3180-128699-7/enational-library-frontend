import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate, Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import 'yup-phone';
import { toast } from 'react-toastify';

import {
  RiMailLine as MailIcon,
  RiLockLine as PassIcon,
  RiAccountCircleLine as AccountIcon,
  RiPhoneLine as PhoneIcon,
} from 'react-icons/ri';

import { login, selectLoggedInState } from 'src/features/authentication/authenSlice';
import { authenAPI } from 'src/features/authentication/authenAPI';

import './Register.css';

const schema = yup.object().shape({
  firstName: yup.string().required('This field is required'),
  lastName: yup.string().required('This field is required'),
  email: yup.string().email('Please enter a valid email').required('This field is required'),
  phone: yup
    .string()
    .required('This field is required')
    .phone('VN', true, 'Phone number has to be valid')
    .max(15, 'Phone number is invalid'),
  password: yup
    .string()
    .min(8, 'Your password must be at least 8 characters')
    .required('This field is required'),
  confirmPassword: yup
    .string()
    .required('This filed is required')
    .oneOf([yup.ref('password'), null], 'Password must match'),
});

const Register = () => {
  const dispatch = useDispatch();

  //global state
  const isLoggedIn = useSelector(selectLoggedInState);
  //local state
  const [checkbox, setCheckBox] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  if (isLoggedIn) {
    return <Navigate to="/" />;
  }

  const submitForm = (data) => {
    authenAPI
      .registerAPI({
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        phone: data.phone,
        password: data.password,
      })
      .then((response) => {
        toast.success('Signup successful. Welcome to ENational Library.', { theme: 'colored' });
        dispatch(login(response.data));
      })
      .catch((error) => {
        toast.error('Signup failed', { theme: 'colored' });
      });
  };

  return (
    <React.Fragment>
      <div className="form-box">
        <div className="login-title">Sign Up</div>
        <div className="login-title-welcome">Welcome to ENational Library</div>

        <div className="form-wrapper">
          <form
            className="group input-form"
            autoComplete="off"
            onSubmit={handleSubmit((data) => submitForm(data))}
          >
            <div className="sign-up-wrapper">
              <div className="input-wrapper-left">
                <div className="input-wrapper-child">
                  <input
                    className="input-field"
                    placeholder="First name"
                    autoComplete="off"
                    {...register('firstName')}
                  />
                  <AccountIcon className="input-icon" />
                </div>
                <p className="login-page-error-message">{errors.firstName?.message}</p>
              </div>

              <div className="input-wrapper-left">
                <div className="input-wrapper-child">
                  <input
                    className="input-field"
                    placeholder="Last name"
                    autoComplete="off"
                    {...register('lastName')}
                  />
                  <AccountIcon className="input-icon" />
                </div>
                <p className="login-page-error-message">{errors.lastName?.message}</p>
              </div>
            </div>

            <div className="sign-up-wrapper">
              <div className="input-wrapper-left">
                <div className="input-wrapper-child">
                  <input
                    type="text"
                    className="input-field"
                    placeholder="Email"
                    autoComplete="off"
                    {...register('email')}
                  />
                  <MailIcon className="input-icon" />
                </div>
                <p className="login-page-error-message">{errors.email?.message}</p>
              </div>

              <div className="input-wrapper-left">
                <div className="input-wrapper-child">
                  <input
                    type="text"
                    className="input-field"
                    placeholder="Phone"
                    autoComplete="off"
                    {...register('phone')}
                  />
                  <PhoneIcon className="input-icon" />
                </div>
                <p className="login-page-error-message">{errors.phone?.message}</p>
              </div>
            </div>

            <div className="input-wrapper">
              <div className="input-wrapper-child">
                <input
                  type="password"
                  className="input-field"
                  placeholder="Password"
                  {...register('password')}
                />
                <PassIcon className="input-icon" />
              </div>
              <p className="login-page-error-message">{errors.password?.message}</p>
            </div>

            <div className="input-wrapper">
              <div className="input-wrapper-child">
                <input
                  type="password"
                  className="input-field"
                  placeholder="Confirm password"
                  {...register('confirmPassword')}
                />
                <PassIcon className="input-icon" />
              </div>
              <p className="login-page-error-message">{errors.confirmPassword?.message}</p>
            </div>

            <input type="checkbox" className="check-box" onClick={() => setCheckBox(!checkbox)} />
            <span className="checkbox-title">Accept Terms and Condition</span>

            <div className="row submit-btn">
              <input
                className="input-submit"
                disabled={!checkbox}
                type="submit"
                title={checkbox ? '' : 'Please tick on Accept the Terms and Conditions'}
              />
            </div>

            <div className="login-link">
              <Link className="signup-link" to="/auth/login">
                Having an account ?
              </Link>
            </div>
          </form>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Register;
