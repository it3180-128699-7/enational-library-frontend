import React from 'react';

import './AuthorInfo.css';

function AuthorInfo({ author }) {
  return (
    <div className="author-info--wrapper" style={{ backgroundImage: `url(${author.avatar})` }}>
      <div className="author-info--background" />

      <div className="row author-info--foreground m-0">
        <div className="col-sm-12 col-md-4 p-0 author-avatar--wrapper text-center">
          <img src={author.avatar} alt="author" className="author-avatar--img" />
        </div>
        <div className="col-sm-12 col-md-8">
          <div className="p-4">
            <h2>{author.name}</h2>
            <p>{author.bio}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AuthorInfo;
