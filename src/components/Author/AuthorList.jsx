import React from 'react';
import { Link } from 'react-router-dom';

import './AuthorList.css';

const AuthorList = ({ authors }) => {
  return (
    <React.Fragment>
      {authors.map((author, key) => {
        return (
          <div key={key} className="col-6 col-sm-4 col-md-3 col-lg-2 py-2">
            <Link className="author-list-item" to={`/authors/${author.id}`}>
              <div className="d-flex flex-column">
                <img className="author-item-img" src={author.avatar} alt="Author" />
                <h4 className="pt-3 text-center author-item-name">{author.name}</h4>
              </div>
            </Link>
          </div>
        );
      })}
    </React.Fragment>
  );
};

export default AuthorList;
