import React from 'react';
import { Link } from 'react-router-dom';

import './BookCard.css';

const BookCard = ({ book }) => {
  return (
    <React.Fragment>
      <div className="col-6 col-sm-4 col-md-3 col-lg-2 p-2">
        <Link to={`/book/${book.id}`} className="card h-100 book-list-item">
          <div className="card-img-top">
            <div
              className="book-item-img"
              style={{ backgroundImage: `url(${book.image})` }}
              title="This is a book image"
            ></div>
          </div>
          <p className="book-item-topic">{book.category.title}</p>
          <p className="book-item-name" title={`${book.title}`}>
            {book.title}
          </p>
          <p className="book-item-author">{book.author.name}</p>
        </Link>
      </div>
    </React.Fragment>
  );
};

export default BookCard;
