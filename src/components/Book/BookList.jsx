import React from 'react';

import BookCard from './BookCard';

const BookList = ({ books = [] }) => {
  return (
    <React.Fragment>
      <div className="row book-list">
        {books.map((book) => (
          <BookCard key={book.id} book={book} />
        ))}
      </div>
    </React.Fragment>
  );
};

export default BookList;
