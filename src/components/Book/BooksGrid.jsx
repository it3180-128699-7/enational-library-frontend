import React from 'react';
import { Row, Col } from 'react-bootstrap';

import BookCard from './BookCard';
import Pagination from 'src/components/Pagination';

export default function BooksGrid({ books, count, filter, setFilter }) {
  return (
    <>
      <Row>
        {books.length &&
          books.map((book) => (
            <Col xs={6} md={3}>
              <BookCard key={book.id} book={book} />
            </Col>
          ))}
      </Row>
      <Row style={{ marginTop: '30px' }}>
        <Col style={{ display: 'flex', justifyContent: 'center' }}>
          <Pagination
            page={filter.page}
            pagesCount={Math.floor(count / filter.limit)}
            setPage={(newPage) => setFilter((filter) => ({ ...filter, page: newPage }))}
          />
        </Col>
      </Row>
    </>
  );
}
