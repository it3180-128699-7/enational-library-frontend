import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import './BookContent.css';
import { addtoCart } from 'src/features/cart/cartSlice';
import { selectUser } from 'src/features/authentication/authenSlice';

const BookContent = ({ book }) => {
  const dispatch = useDispatch();
  const userData = useSelector(selectUser);
  const [cartNotify, setCartNotify] = useState('');

  if (localStorage.getItem('cart') === null) {
    localStorage.setItem('cart', '[]');
  }
  const cart = JSON.parse(localStorage.getItem('cart'));

  const addToCart = () => {
    if (!(book.isExclusive && !userData.account.isVip)) {
      if (cart.length === 0) {
        cart.push({
          id: 1,
          bookId: book.id,
          bookJson: book,
        });
        localStorage.setItem('cart', JSON.stringify(cart));
        setCartNotify('Book is successfully added to your cart');
        dispatch(addtoCart());
      } else {
        if (
          cart.every((cartItem) => {
            return cartItem.bookId !== book.id;
          })
        ) {
          let maxId = 0;
          for (let i = 0; i < cart.length; i++) {
            if (maxId < cart[i].id) maxId = cart[i].id;
          }
          cart.push({
            id: maxId + 1,
            bookId: book.id,
            bookJson: book,
          });
          localStorage.clear();
          localStorage.setItem('cart', JSON.stringify(cart));
          setCartNotify('Book is successfully added to your cart');
          dispatch(addtoCart());
        } else {
          setCartNotify('Unsuccessful! This book is already in your cart');
        }
      }
    }
  };

  return (
    <React.Fragment>
      {/* Tài khoản hợp lệ */}
      <div
        className="modal fade"
        id="acceptAccountModal"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{cartNotify}</h5>
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary" data-bs-dismiss="modal">
                Continue shopping
              </button>
              <Link to="/cart">
                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">
                  Checkout now!
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>

      {/* tài khoản cần nâng cấp vip */}
      <div
        className="modal fade"
        id="denyAccountModal"
        tabIndex="-2"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 style={{ textAlign: 'center' }} className="modal-notif">
                Sorry! This book is only for Vip account
              </h5>
              <button
                style={{ backgroundColor: 'white' }}
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-footer">
              <Link to="/">
                <button type="button" className="btn btn-primary" data-bs-dismiss="modal">
                  Update your account
                </button>
              </Link>
              <Link to="/cart">
                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">
                  Checkout now!
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div className="bookdetail-content">
        <div className="row bookdetail-content--foreground p-5">
          <div className="col-12 col-sm-12 col-md-5">
            <img className="bookdetail-item-img" src={book.image} alt="imagebook" />
          </div>

          <div className="col-12 col-sm-12 col-md-7 book-infor pt-5">
            <h1 className="book-name">{book.title}</h1>
            <div className="book-author">
              <p className="author-name">{book.author.name}</p>
            </div>
            <div className="mb-3">
              <p style={{ color: '#ecf0f1' }}>{book.description}</p>
              <span className={`badge bg-${book.isExclusive ? 'warning' : 'primary'}`}>
                {book.isExclusive ? 'Exclusive book' : 'Normal Book'}
              </span>
            </div>

            <button
              onClick={addToCart}
              type="button"
              className="book-infor"
              data-bs-toggle="modal"
              data-bs-target={`#${
                book.isExclusive === true && userData.account.isVip === false
                  ? 'denyAccountModal'
                  : 'acceptAccountModal'
              }`}
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default BookContent;
