import React from 'react';

import BookContent from './BookContent';
import BookReview from './BookReview';
import OtherBookList from './OtherBookList';

const BookDetail = ({ book }) => {
  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col">
            <BookContent book={book} />
          </div>
        </div>

        <div className="row">
          <div className="col-sm-12 col-md-8">
            <BookReview book={book} />
          </div>

          <div className="col-sm-12 col-md-4 pt-3  otherbook">
            <div className="otherbook-heading pb-3">Maybe you like</div>
            <ul className="otherbook-list">
              <OtherBookList book={book}></OtherBookList>
            </ul>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default BookDetail;
