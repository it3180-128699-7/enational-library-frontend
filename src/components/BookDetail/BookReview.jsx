import React, { useState } from 'react';

import './BookReview.css';

const BookReview = ({ book }) => {
  const [tab, setTab] = useState(1);
  const touchState = (index) => {
    setTab(index);
  };

  return (
    <div>
      <div className="bookdetail-tabs tabs">
        <div className={tab === 1 ? 'tab-item active' : 'tab-item'} onClick={() => touchState(1)}>
          Description
        </div>
        <div className={tab === 2 ? 'tab-item active' : 'tab-item'} onClick={() => touchState(2)}>
          More Information
        </div>
      </div>
      <div className="tab-content">
        <div className={tab === 1 ? 'tab-pane active bookdetail-tab' : 'tab-pane bookdetail-tab'}>
          <p className="bookdetail-line">- Category: {book.category.title}</p>
          <p className="bookdetail-line">- Page number: {book.pageNumber} pages</p>
          <p className="bookdetail-line">- Stock: {book.stock} books</p>
        </div>
        <div className={tab === 2 ? 'tab-pane active bookdetail-tab' : 'tab-pane bookdetail-tab'}>
          <p className="bookdetail-line">- Publisher: {book.publisher}</p>
          <p className="bookdetail-line">- Published Date: {book.publishedDate.slice(0, 10)}</p>
        </div>
      </div>
    </div>
  );
};

export default BookReview;
