import React from 'react';
import { Link } from 'react-router-dom';

const OtherBook = ({ book }) => {
  return (
    <React.Fragment>
      <div className="media">
        <img className="otherbook-img" src={book.image} alt="bookimage" />
        <div className="otherbook-infor">
          <Link to={`/book/${book.id}`} className="otherbook-name">
            {book.title}
          </Link>
          <p className="otherbook-author">{book.author.name}</p>
        </div>
      </div>
    </React.Fragment>
  );
};
export default OtherBook;
