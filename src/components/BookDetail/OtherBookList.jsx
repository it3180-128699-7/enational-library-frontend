import React, { useState, useEffect } from 'react';
import _ from 'lodash';

import bookApi from 'src/features/book/bookApi';
import { getToken } from 'src/utils/tokenUtil';
import OtherBook from './OtherBook';

const OtherBookList = ({ book }) => {
  const [otherBooks, setOtherBooks] = useState([]);

  useEffect(() => {
    async function fetchOtherBooks() {
      const sameCateBooksResponse = await bookApi.getBooks(
        { limit: 4, category: book.category.id },
        getToken()
      );
      const sameAuthorBooksResponse = await bookApi.getBooks(
        { limit: 4, author: book.author.id },
        getToken()
      );
      const combineBooks = _.uniqBy(
        [...sameCateBooksResponse.data.items, ...sameAuthorBooksResponse.data.items].filter(
          (otherBook) => otherBook.id !== book.id
        ),
        'id'
      ).slice(0, 4);
      setOtherBooks(combineBooks);
    }
    fetchOtherBooks();
  }, [book.author.id, book.category.id, book.id]);

  return (
    <React.Fragment>
      {otherBooks.map((other_book, key) => (
        <li className="mb-5" key={key}>
          <OtherBook key={other_book.index} book={other_book} />
        </li>
      ))}
    </React.Fragment>
  );
};

export default OtherBookList;
