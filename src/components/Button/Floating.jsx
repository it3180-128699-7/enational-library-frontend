import React from 'react';

import './Button.css';

function FloatingButton({ onClick, children }) {
  return (
    <div
      className="floating-button d-flex justify-content-center align-items-center"
      onClick={onClick}
    >
      {children}
    </div>
  );
}

export default FloatingButton;
