import React from 'react';

import './Button.css';

const Button = (props) => {
  return (
    <React.Fragment>
      <button className="button" onClick={props.handleEvent}>
        {props.title}
      </button>
    </React.Fragment>
  );
};

export default Button;
