import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import './CartComponentCSS/CartBook.css';
import Icon from 'src/assets/icons/cross.png';
import { deleteItem } from 'src/features/cart/cartSlice';

const CartBook = ({ bookItem }) => {
  const dispatch = useDispatch();
  //API get book{id}
  const [book] = useState(bookItem);
  /*useEffect(() => {
    async function fetchGetItem() {
      const bookResponse = await cartApi.getBook(bookItem.bookId);
      setBook(bookResponse.data);
      console.log(bookItem.bookId);
    }

    fetchGetItem();
  }, [bookItem]);*/

  return (
    <React.Fragment>
      <div className="cart-item" style={{ textDecoration: 'none' }}>
        <div className="book-row">
          <Link to={`/book/${book.bookId}`}>
            <div className="book-row-description">
              <img className="cart-item-img" src={book.bookJson.image} alt="" />
              <div className="cart-item-detail">
                <p className="cart-item-topic">{book.bookJson.category.title || ''}</p>
                <p className="cart-item-name">{book.bookJson.title || ''}</p>
                <p className="cart-item-author">{book.bookJson.author.name || ''}</p>
                <p className="cart-item-status">
                  {book.status === 1
                    ? `${book.stock} item left`
                    : book.stock > 1
                    ? `${book.stock} items left`
                    : 'Out of order'}
                </p>
              </div>
            </div>
          </Link>
          <img
            className="container cart-item-cross"
            src={Icon}
            alt=""
            onClick={() => dispatch(deleteItem(bookItem))}
          />
        </div>
      </div>
    </React.Fragment>
  );
};

export default CartBook;
