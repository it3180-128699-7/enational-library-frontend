import React from 'react';
import { useSelector } from 'react-redux';

import './CartComponentCSS/CartContent.css';
import CartOrderList from './CartOrderList';

const CartPage = (props) => {
  const cartList = useSelector((state) => state.cart);

  return (
    <React.Fragment>
      <div className="cart-content">
        <div className="container cart-heading">
          <div className="row">
            <div className="col-12 col-md-8 col-lg-8">
              <div className="cart-heading-feature_name">Your Cart</div>
              <div className="cart-heading-description">Please check your cart before checkout</div>
            </div>
            <div className="col-12 col-md-4 col-lg-4"></div>
          </div>
        </div>
        {/*Order list*/}
        <div className="container cart-orderlist">
          <div className="row main">
            <CartOrderList cartList={cartList} />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default CartPage;
