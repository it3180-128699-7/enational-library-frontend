import React from 'react';
import './CartComponentCSS/CartOrderList.css';
import CartBook from './CartBook';
import CartOrderSubmit from './CartOrderSubmit';

const CartOrderList = ({ cartList }) => {
  const books = cartList;

  return (
    <React.Fragment>
      <div className="col-12 col-md-8 col-lg-8 cart-books">
        <ul className="cart-list">
          {books.map((book) => (
            <CartBook key={book.id} bookItem={book} />
          ))}
        </ul>
      </div>

      <CartOrderSubmit amount={books.length} />
    </React.Fragment>
  );
};

export default CartOrderList;
