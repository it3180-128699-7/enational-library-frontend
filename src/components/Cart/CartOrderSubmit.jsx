import React from 'react';
import { Link } from 'react-router-dom';

import './CartComponentCSS/CartOrderSubmit.css';

const CartOrderSubmit = ({ amount }) => {
  return (
    <React.Fragment>
      <div className="col-12 col-md-4 col-lg-4 cart-total">
        <div className="cart-total-info">
          <p className="cart-total-label">Total:</p>
          <p className="cart-total-value">{amount > 1 ? `${amount} Items` : `${amount} Item`}</p>
        </div>
        {/*Description*/}
        <div className="cart-total-description">
          <p className="cart-total-description-1">
            Please review your cart carefully before checkout. Check your items, fill your address
            correctly and we will contact you within 1 day.
          </p>
          <p className="cart-total-description-2">
            Before checkout, please ensure you are an registered-member and owned the membership
            card. For more information, please access "Member" section and look for your register
            status. Any items lost or damaged will not be accepted and you have to pay more 15% of
            item cost.
          </p>
        </div>
        {/*Button*/}
        <div className="cart-buttons">
          <Link to="/payment">
            <button className="cart-checkout-button" disabled={!amount}>
              Checkout
            </button>
          </Link>
        </div>
        <div>
          <Link to="/category">
            <button className="cart-continue-button">Add more item</button>
          </Link>
        </div>
      </div>
    </React.Fragment>
  );
};

export default CartOrderSubmit;
