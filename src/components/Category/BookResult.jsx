import React from 'react';
import { Row, Col } from 'react-bootstrap';

import BookCard from 'src/components/Book/BookCard';
import Pagination from 'src/components/Pagination';
import NoData from 'src/components/NoData';

const BookResult = ({ books, count, filter, setFilter }) => {
  return books.length === 0 ? (
    <div className="container">
      <div className="row">
        <div className="col d-flex justify-content-center">
          <NoData width={500} height={500} />
        </div>
      </div>
    </div>
  ) : (
    <React.Fragment>
      <div class="container results-books-list">
        <div className="row book-list">
          {books.map((book) => (
            <BookCard key={book.id} book={book} />
          ))}
        </div>
      </div>

      <Row>
        <Col className="d-flex justify-content-center">
          <Pagination
            page={filter.page}
            pagesCount={Math.ceil(count / filter.limit)}
            setPage={(newPage) => {
              setFilter({ ...filter, page: newPage });
            }}
          />
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default BookResult;
