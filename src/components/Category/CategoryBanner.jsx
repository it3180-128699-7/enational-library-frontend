import React from 'react';

import './CategoryCompoCSS/CategoryBanner.css';
import SearchBar from './SearchBar';

const CategoryBanner = ({ filter, setFilter }) => {
  return (
    <React.Fragment>
      <div className="category-banner py-5 px-3">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-sm-12 md-8">
              <div className="categories-header">
                <h2 className="categories-description--1">
                  Just type the title. We care the remain!
                </h2>
              </div>
            </div>
          </div>

          <div className="row mb-3 justify-content-center">
            <div className="col-sm-12 md-8">
              <p className="categories-description--2">
                We have the millions of books available right now. To see you favorite book, type
                the title here and press Enter
              </p>
            </div>
          </div>

          <div className="row justify-content-center">
            <div className="col-sm-12 col-md-8">
              <SearchBar filter={filter} setFilter={setFilter} />
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default CategoryBanner;
