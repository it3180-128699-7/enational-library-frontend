import React from 'react';
import { useSelector } from 'react-redux';

import BookResult from './BookResult';
import FilterList from './FilterList';
import DropDown from './DropDown';
import LoadingPage from 'src/components/Loading/Loading';

import { selectBooks, selectBooksCount, selectBookAPIState } from 'src/features/book/bookSlice';
import './CategoryCompoCSS/CategoryBody.css';

const CategoryBody = ({ filter, setFilter }) => {
  // global state
  const booksCount = useSelector(selectBooksCount);
  const books = useSelector(selectBooks);
  const isFetching = useSelector(selectBookAPIState);

  return (
    <React.Fragment>
      <div className="container books">
        <div className="row gx-0">
          <div className="col-12 col-sm-4 col-lg-2">
            <FilterList filter={filter} setFilter={setFilter} />
          </div>

          <div className="col-12 col-sm-8 col-lg-10">
            <div className="results p-3">
              <div className="results-option">
                <DropDown count={booksCount} filter={filter} setFilter={setFilter} />
                {!isFetching ? (
                  <BookResult
                    books={books}
                    count={booksCount}
                    filter={filter}
                    setFilter={setFilter}
                  />
                ) : (
                  <LoadingPage />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default CategoryBody;
