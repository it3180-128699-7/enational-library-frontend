import React, { useState } from 'react';
import { FcCollapse as CollapseIcon, FcExpand as ExpandIcon } from 'react-icons/fc';

import './CategoryCompoCSS/Collapsible.css';

const Collapsible = (props) => {
  const [isOpen, setOpen] = useState(true);

  return (
    <React.Fragment>
      <div className="filter-pane-list">
        <div
          className="filter-pane-label d-flex justify-content-between align-items-center mb-3"
          onClick={() => setOpen(!isOpen)}
        >
          <div className="filter-pane-categories-name">{props.label}</div>
          <div className="filter-pane-categories-icon">
            {isOpen ? <CollapseIcon /> : <ExpandIcon />}
          </div>
        </div>
        <ul className="filter-pane-categories-list">
          {isOpen && <React.Fragment>{props.children}</React.Fragment>}
        </ul>
      </div>
    </React.Fragment>
  );
};

export default Collapsible;
