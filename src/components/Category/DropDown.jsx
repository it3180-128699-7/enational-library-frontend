import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import { fetchGetBooks } from 'src/features/book/bookSlice';

import './CategoryCompoCSS/DropDown.css';

const DropDown = ({ count, filter, setFilter }) => {
  const dispatch = useDispatch();
  const [option, setOption] = useState('Default sorting');

  const dropDownOptions = [
    {
      id: 0,
      title: 'Default sorting',
    },
    {
      id: 1,
      title: 'Sort by oldest first',
    },
    {
      id: 2,
      title: 'Sort by newest first',
    },
    {
      id: 3,
      title: 'Sort by Title from A to Z',
    },
    {
      id: 4,
      title: 'Sort by Title from Z to A',
    },
    {
      id: 5,
      title: 'Sort by popularity',
    },
  ];

  const fetchBookByOption = (e, id) => {
    e.preventDefault();
    setOption(e.target.textContent);
    dispatch(fetchGetBooks({ ...filter, sort: id }));
    setFilter({ ...filter, sort: id });
  };

  return (
    <React.Fragment>
      <div className="results-option">
        <div className="results-label mb-2">
          {count === 1 ? 'Showing a single result' : `Showing all ${count} results`}
        </div>
        <div className="results-dropdown mb-2">
          <button
            className="btn dropdown-toggle"
            type="button"
            id="dropdownMenuButton1"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            {option}
          </button>
          <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            {dropDownOptions.map((item) => (
              <li key={item.id}>
                <button className="dropdown-item" onClick={(e) => fetchBookByOption(e, item.id)}>
                  {item.title}
                </button>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </React.Fragment>
  );
};

export default DropDown;
