import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { fetchAllCategories, selectCategories } from 'src/features/category/categorySlice';
import { fetchAllAuthors, selectAuthors } from 'src/features/author/authorSlice';

import Collapsible from './Collapsible';
import FilterResult from './FilterResult';

import './CategoryCompoCSS/FilterList.css';

const FilterList = ({ filter, setFilter }) => {
  const dispatch = useDispatch();

  //global state
  const cateList = useSelector(selectCategories);
  const authorList = useSelector(selectAuthors);
  // global state
  const bookStatus = [
    {
      id: 1,
      description: 'Exclusive',
    },
    {
      id: 2,
      description: 'Non-Exclusive',
    },
    {
      id: 3,
      description: 'All',
    },
  ];

  useEffect(() => {
    dispatch(fetchAllCategories());
    dispatch(fetchAllAuthors());
  }, [dispatch]);

  return (
    <React.Fragment>
      <div className="filter-pane p-3">
        <Collapsible label="Categories">
          {cateList.map((category) => {
            return (
              <FilterResult
                id={category.id}
                title={category.title}
                keyName="category"
                filter={filter}
                setFilter={setFilter}
              />
            );
          })}
        </Collapsible>

        <Collapsible label="Authors">
          {authorList.map((author) => {
            return (
              <FilterResult
                id={author.id}
                title={author.name}
                keyName="author"
                filter={filter}
                setFilter={setFilter}
              />
            );
          })}
        </Collapsible>

        <Collapsible label="Exclusive">
          {bookStatus.map((exclusive) => {
            return (
              <FilterResult
                id={exclusive.id}
                title={exclusive.description}
                keyName="exclusive"
                filter={filter}
                setFilter={setFilter}
              />
            );
          })}
        </Collapsible>
      </div>
    </React.Fragment>
  );
};

export default FilterList;
