import React, { useState, useEffect } from 'react';

const FilterResult = ({ id, title, keyName, filter, setFilter, currentID }) => {
  const [clicked, setClicked] = useState(true);

  const handleClicked = () => {
    if (!clicked) {
      setFilter({ ...filter, [keyName]: id });
    } else {
      setFilter({ ...filter, [keyName]: 0 });
    }
    setClicked(!clicked);
  };

  useEffect(() => {
    setClicked(filter[keyName] === id);
  }, [filter, id, keyName]);

  return (
    <li key={id} className="mb-1">
      <div
        className={`filter-pane-categories-item-link ${clicked ? 'active' : ''}`}
        onClick={handleClicked}
      >
        {title}
      </div>
    </li>
  );
};

export default FilterResult;
