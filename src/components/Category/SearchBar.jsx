import React, { useState, useEffect, useRef } from 'react';
import { FaSearch as SearchIcon } from 'react-icons/fa';

import './CategoryCompoCSS/SearchBar.css';

const SearchBar = ({ filter, setFilter }) => {
  const [input, setInput] = useState('');
  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const handleChange = (e) => {
    setInput(e.target.value);
  };

  const searchByKeyWord = (e) => {
    e.preventDefault();
    setFilter({ ...filter, search: input });
    setInput('');
  };

  return (
    <React.Fragment>
      <div className="search-bar-input p-2">
        <button className="search-bar-search-icon-button pe-3" onClick={searchByKeyWord}>
          <SearchIcon className="search-bar-search-icon" />
        </button>

        <form className="search-bar-form" onSubmit={searchByKeyWord}>
          <input
            type="text"
            placeholder="Search for book..."
            value={input}
            className="search-bar"
            onChange={handleChange}
            ref={inputRef}
          />
        </form>
      </div>
    </React.Fragment>
  );
};

export default SearchBar;
