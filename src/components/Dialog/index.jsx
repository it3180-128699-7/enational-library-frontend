import React from 'react';
import { Modal, Button } from 'react-bootstrap';

function Dialog({
  show,
  setShow,
  title,
  body,
  buttonTitle1,
  buttonAction1,
  buttonTitle2,
  buttonAction2,
}) {
  const handleClose = () => setShow(false);

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>{title && <Modal.Title>{title}</Modal.Title>}</Modal.Header>
      {body && <Modal.Body>{body}</Modal.Body>}
      <Modal.Footer>
        <Button variant="secondary" onClick={buttonAction1}>
          {buttonTitle1 || 'Close'}
        </Button>
        <Button variant="primary" onClick={buttonAction2}>
          {buttonTitle2 || 'Save'}
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default Dialog;
