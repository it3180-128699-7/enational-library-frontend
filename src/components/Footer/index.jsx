import React from 'react';
import { Link } from 'react-router-dom';
import {
  FiNavigation as LocationIcon,
  FiMail as MailIcon,
  FiPhone as PhoneIcon,
} from 'react-icons/fi';

import './Footer.css';

const Footer = () => {
  return (
    <React.Fragment>
      <div className="footer py-4">
        <div className="container">
          <div className="row">
            <div className=" col-sm-4 col-md col-sm-4 col-12 mb-2">
              <h5 className="col-heading">ENATIONAL LIBRARY</h5>
              <ul>
                <li className="d-flex align-items-center">
                  <LocationIcon />
                  <div className="ms-2">Hai Ba Trung, Ha Noi</div>
                </li>
                <li className="d-flex align-items-center">
                  <PhoneIcon />
                  <div className="ms-2">
                    <a href="tel:+84999999999">+8499999999</a>
                  </div>
                </li>
                <li className="d-flex align-items-center">
                  <MailIcon />
                  <div className="ms-2">
                    <a href="mailto:admin@enlibrary.gay">admin@enlibrary.gay</a>
                  </div>
                </li>
              </ul>
            </div>

            <div className=" col-sm-4 col-md col-6 mb-2">
              <h5 className="col-heading">Explorer</h5>
              <ul>
                <li>
                  <div>
                    <a href="/">About Us</a>
                  </div>
                </li>
              </ul>
            </div>

            <div className=" col-sm-4 col-md col-6 mb-2">
              <h5 className="col-heading">Customer Services</h5>
              <ul>
                <li>
                  <div>
                    <a href="/profile">Vip Member</a>
                  </div>
                </li>
                <li>
                  <div>
                    <a href="mailto:admin@enlibrary.gay">Help center</a>
                  </div>
                </li>
              </ul>
            </div>

            <div className=" col-sm-4 col-md col-12 mb-2">
              <h5 className="col-heading">Categories</h5>
              <ul>
                <li>
                  <Link to="/category">Action</Link>
                </li>
                <li>
                  <Link to="/category">Comedy</Link>
                </li>
                <li>
                  <Link to="/category">Drama</Link>
                </li>
                <li>
                  <Link to="/category">Horror</Link>
                </li>
                <li>
                  <Link to="/category">Kids</Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <p className="text-center">Copyright ©2022 | Designed by Nhosm Se7ven</p>
      </div>
    </React.Fragment>
  );
};

export default Footer;
