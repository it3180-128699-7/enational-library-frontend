import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
// images and icons
import { BsCart2 as CartIcon, BsBoxArrowRight as LogOutIcon } from 'react-icons/bs';

import LibraryIcon from 'src/assets/icons/library.ico';
import { navItemList } from 'src/configs/constant.js';

import userCheckLogin from 'src/hooks/useCheckLogin';
import { selectUser, logout } from 'src/features/authentication/authenSlice';

import './Header.css';

const Header = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  userCheckLogin(location.pathname);
  const user = useSelector(selectUser);

  return (
    <React.Fragment>
      <div className="header sticky-top shadow-lg">
        <nav className="navbar navbar-expand-lg navbar-light menubar">
          <h1 className="navbar-brand">
            <Link to="/">
              <img
                src={LibraryIcon}
                alt="Library Icon"
                width="30"
                height="100%"
                className="d-inline-block logo"
              />
              ENational Library
            </Link>
          </h1>

          {/* button show item list when the ratio of screen is reduced */}
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          {/* navbar lít */}
          <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              {navItemList.map((item) => {
                return (
                  <li key={item.id} className={item.itemHide ? 'nav-item hide-nav' : 'nav-item'}>
                    <Link
                      className={`nav-link ${location.pathname === item.linkRef ? 'active' : ''}`}
                      style={{ fontWeight: 500 }}
                      aria-current="page"
                      to={item.linkRef}
                    >
                      {item.title}
                    </Link>
                  </li>
                );
              })}
              <li className="nav-item hide-nav">
                <Link
                  style={{ fontWeight: 500 }}
                  aria-current="page"
                  onClick={(e) => {
                    e.preventDefault();
                    dispatch(logout());
                  }}
                  to="/"
                >
                  Log out
                </Link>
              </li>
            </ul>

            {/* Shopping cart and User icon */}
            <div className="d-flex nav-icon align-items-center">
              <div style={{ fontSize: 20 }}>Hello,</div>
              <Link to="/profile" className="navbar-icon d-flex align-items-center">
                <div className="me-2">{`${user?.firstName} ${user?.lastName}`}</div>
                <img
                  src={user?.avatar}
                  width={40}
                  height={40}
                  alt="avatar"
                  className="rounded-circle"
                  style={{ objectFit: 'cover' }}
                />
              </Link>
              <Link to="/cart" className="navbar-icon">
                <CartIcon />
              </Link>
              <Link
                className="navbar-icon"
                onClick={(e) => {
                  e.preventDefault();
                  dispatch(logout());
                }}
                to="/"
              >
                <LogOutIcon />
              </Link>
            </div>
          </div>
        </nav>
      </div>
    </React.Fragment>
  );
};

export default Header;
