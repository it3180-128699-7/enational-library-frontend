import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';

import authorApi from 'src/features/author/authorApi';
import AuthorList from 'src/components/Author/AuthorList';
import Button from 'src/components/Button';

const HomeAuthors = () => {
  const [authors, setAuthors] = useState([]);

  useEffect(() => {
    const fetchAuthors = async () => {
      const authorsResponse = await authorApi.getAuthors();
      setAuthors(authorsResponse.data.slice(0, 6));
    };
    fetchAuthors();
  }, []);
  const navigate = useNavigate();

  return (
    <React.Fragment>
      {/* Authors title */}
      <div className="container authors mt-5">
        <div className="content-heading">
          <h2 className="section-heading author-section">Favorite Authors</h2>
          <p className="section-sub-heading author-sub-heading"></p>
        </div>
      </div>

      <div className="container">
        <div className="row">
          {/* list of author */}
          <AuthorList authors={authors} />
        </div>
        <div className="row">
          <div className="col d-flex justify-content-center">
            <Button title="See more" handleEvent={() => navigate('/authors')} />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default HomeAuthors;
