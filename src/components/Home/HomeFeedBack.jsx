import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import Button from 'src/components/Button';
import { toast } from 'react-toastify';

import { fetchPostFeedBack } from 'src/features/feedback/feedBackSlice';

import './HomeComponentsCSS/HomeFeedBack.css';

const HomeFeedBack = () => {
  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [message, setMessage] = useState('');

  const sentFeedBack = () => {
    if (email && name && message) {
      dispatch(
        fetchPostFeedBack({
          name: name,
          email: email,
          content: message,
        })
      );
      toast.success('Submit successful');
      setName('');
      setEmail('');
      setMessage('');
    } else {
      toast.error('You have to fill all fields');
    }
  };

  return (
    <React.Fragment>
      {/* Form Feedback */}
      <div className="container feedback mt-5">
        <div className="content-heading">
          <h2 className="section-heading feedback-section">Join Our Newsletter</h2>
          <p className="section-sub-heading feedback-sub-heading">
            Fill your email below to get the latest updates of our books
          </p>
        </div>
        <form action="#">
          <div className="feedback-form">
            <div className="row form-infor">
              <div className="col-12 col-sm-7 col-md-7 mb-3">
                <input
                  type="email"
                  value={email}
                  placeholder="Enter your Email"
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <div className="col-12 col-sm-5 col-md-5">
                <input
                  value={name}
                  placeholder="Enter your full name"
                  onChange={(e) => setName(e.target.value)}
                />
              </div>
            </div>

            <div className="form-content">
              <textarea
                rows="5"
                value={message}
                placeholder="Your Message"
                onChange={(e) => setMessage(e.target.value)}
              />
            </div>
            {/* Click on submit button will response some message */}
            <Button
              title="Submit"
              handleEvent={(e) => {
                e.preventDefault();
                sentFeedBack();
              }}
            />
          </div>
        </form>
      </div>
    </React.Fragment>
  );
};

export default HomeFeedBack;
