import React from 'react';

import './LoadingScreen.css';

const LoadingScreen = () => {
  return (
    <div className="loading-screen">
      <header className="loading-header">
        <img src="./logo512.png" className="app-logo" alt="logo" />
        <p className="loading-title">Welcome to ENational Library Website</p>
      </header>
    </div>
  );
};

export default LoadingScreen;
