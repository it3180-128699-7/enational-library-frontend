import React from 'react';

import maintenanceImg from 'src/assets/images/maintenance.svg';

function Maintenance(props) {
  return <img src={maintenanceImg} alt="In maintenance" {...props} />;
}

export default Maintenance;
