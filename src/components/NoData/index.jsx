import React from 'react';

import noDataImg from 'src/assets/images/no-data.svg';

function NoData(props) {
  return <img src={noDataImg} alt="No data to display" {...props} />;
}

export default NoData;
