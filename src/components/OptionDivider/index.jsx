import React from 'react';

function OptionDivider() {
  return <option disabled>-----------------------------</option>;
}

export default OptionDivider;
