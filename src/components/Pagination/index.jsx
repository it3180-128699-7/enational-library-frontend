import { Pagination } from 'react-bootstrap';
import { FaChevronLeft as PrevIcon, FaChevronRight as NextIcon } from 'react-icons/fa';

import './style.css';

export default function CustomPagination({ pagesCount, page, setPage, ...rest }) {
  let items = [];
  for (let index = 1; index <= pagesCount; index++) {
    items.push(
      <Pagination.Item
        key={index}
        active={index === page}
        onClick={() => setTimeout(() => setPage(index), 1000)}
        activeLabel=""
      >
        {index}
      </Pagination.Item>
    );
  }

  return (
    <Pagination {...rest}>
      <Pagination.Item disabled={page === 1} onClick={() => setPage(page - 1)}>
        <PrevIcon />
      </Pagination.Item>
      {items}
      <Pagination.Item disabled={page === pagesCount} onClick={() => setPage(page + 1)}>
        <NextIcon />
      </Pagination.Item>
    </Pagination>
  );
}
