import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { toast } from 'react-toastify';
import { useDispatch } from 'react-redux';

import './PaymentContent.css';
import { getToken } from 'src/utils/tokenUtil';
import paymentAPI from 'src/features/payment/paymentAPI';
import { submitOrderCart } from 'src/features/cart/cartSlice';

const phoneRegExp = /^((\\+[1-9]{1,4})|(\\([0-9]{2,3}\\))|([0-9]{2,4}))*?[0-9]{3,4}?[0-9]{3,4}?$/;
const schema = yup.object().shape({
  name: yup.string().required('This field is required'),
  email: yup.string().email('Please enter a valid email').required('This field is required'),
  address: yup.string().required('This field is required'),
  phone: yup
    .string()
    .matches(phoneRegExp, 'Please enter a valid phone number')
    .required('This field is required'),
  checkbox: yup.boolean(),
});

const cartBooks = () => {
  if (!localStorage.getItem('cart')) {
    localStorage.setItem('cart', '[]');
    return [];
  }
  return JSON.parse(localStorage.getItem('cart')).map((book) => book.bookId);
};

const PaymentContent = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [books] = useState(cartBooks());

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  const submitOrder = (data) => {
    const token = getToken();
    if (token) {
      async function newOrder(token, data) {
        try {
          const response = await paymentAPI.createOrder(token, {
            name: data.name,
            address: data.address,
            phone: data.phone,
            books: books,
          });
          if (response) {
            toast.success('Your order is submited', { theme: 'colored' });
            localStorage.removeItem('cart');
            dispatch(submitOrderCart());
            return navigate('/');
          }
        } catch (error) {
          toast.error(`Submit fail! ${error.response.data?.message}`);
        }
      }
      newOrder(token, data);
    } else {
      toast.error('You have to login before checkout!');
      return navigate('/auth/login');
    }
  };

  const [check, setCheck] = useState(false);

  return (
    <div className="container">
      <div className="row payment-main">
        <div className="col-sm-12 col-md-2 col-lg-2"></div>
        <div className="col-sm-12 col-md-8 col-lg-8 payment-content">
          <div className="payment-heading">
            <div className="payment-heading-feature_name">Order</div>
            <div className="payment-heading-description">
              Please fill your information before checkout
            </div>
          </div>
          <div className="payment-pane active">
            <form action="" className="payment-form" method="">
              <div className="form-group">
                <div className="payment-label">Your name</div>
                <input
                  type="text"
                  name="name"
                  id=""
                  placeholder="Full name"
                  className="form-name"
                  autoComplete="off"
                  {...register('name')}
                />
                <p className="login-page-error-message">{errors.name?.message}</p>
              </div>
              <div className="form-group">
                <div className="payment-label">Email</div>
                <input
                  type="email"
                  name="email"
                  id=""
                  placeholder="abc@gmai.com"
                  className="form-email"
                  autoComplete="off"
                  {...register('email')}
                />
                <p className="login-page-error-message">{errors.email?.message}</p>
              </div>
              <div className="form-group">
                <div className="payment-label">Address</div>
                <input
                  type="text"
                  name="address"
                  id=""
                  placeholder="1 Hacker Way, USA"
                  className="form-address"
                  autoComplete="off"
                  {...register('address')}
                />
                <p className="login-page-error-message">{errors.address?.message}</p>
              </div>
              <div className="form-group">
                <div className="payment-label">Phone number</div>
                <input
                  type="text"
                  name="phone"
                  id=""
                  placeholder="0123456789"
                  className="form-phone"
                  autoComplete="off"
                  {...register('phone')}
                />
                <p className="login-page-error-message">{errors.phone?.message}</p>
              </div>
              {/*<div className="form-group service">
                  <div className="payment-label" style={{ marginTop: '15px' }}>
                    Delivery service:{' '}
                  </div>
                  <div className="btn-group">
                    <button
                      type="button"
                      className="delivery-service btn dropdown-toggle"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      {ServiceList[0].name}
                    </button>
                    <ul className="dropdown-menu dropdown-menu-end">
                      {ServiceList.map((item) => (
                        <li>
                          <button className="dropdown-item" type="button" id={item.id}>
                            {item.name}
                          </button>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>*/}
            </form>
          </div>
          <hr className="solid" />
          <div className="payment-total">
            Total: {books.length < 10 ? `0${books.length} ` : `${books.length} `}
            {books.length > 1 ? `Items` : `Item`}
          </div>
          <div className="payment-notice-description">
            Please check your order carefully before checkout. Review order information, fill
            correctly delivery address and we will contact you within 1 day.
          </div>
          <form action="" className="payment-form" method="" onSubmit={handleSubmit(submitOrder)}>
            <input type="checkbox" className="payment-commit" onChange={() => setCheck(!check)} /> I
            agree to the terms & conditions
            <button type="submit" className="form-submit" disabled={!check || books.length < 1}>
              Checkout
            </button>
          </form>
        </div>
        <div className="col-sm-12 col-md-2 col-lg-2"></div>
      </div>
    </div>
  );
};

export default PaymentContent;
