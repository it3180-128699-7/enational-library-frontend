import React from 'react';

import './Avatar.css';

import avatarApi from 'src/features/user/avatarApi';

const Avatar = (props) => {
  const uploadImage = async (e) => {
    const files = e.target.files;
    const data = new FormData();
    data.append('file', files[0]);
    data.append('upload_preset', 'kieennt');
    const res = await avatarApi.changeAvatarApi(data);

    props.changeAvatar(res.data.secure_url);
  };

  return (
    <React.Fragment>
      <div className="avatar">
        <div className="profile-heading-avatar">
          <img src={props.imgSource} className="profile-heading-avatar-image" alt="User avatar" />
        </div>
        <input type="file" onChange={uploadImage} />
      </div>
    </React.Fragment>
  );
};

export default Avatar;
