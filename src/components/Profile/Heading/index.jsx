import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { toast } from 'react-toastify';

import { getToken } from 'src/utils/tokenUtil';
import { selectUser, changeUserData } from 'src/features/authentication/authenSlice';
import { changeUserAvatarAPI } from 'src/features/authentication/userAPI';

import Avatar from '../Avatar';

const ProfileHeading = () => {
  const dispatch = useDispatch();
  //global state
  const user = useSelector(selectUser);

  const changeAvatar = (imgUrl) => {
    changeUserAvatarAPI(
      {
        avatar: imgUrl,
      },
      getToken()
    )
      .then((response) => {
        if (response) {
          toast.success('Change avatar successful.');
          dispatch(changeUserData(response.data));
        }
      })
      .catch((err) => {
        toast.error(err.response?.data?.message, { theme: 'colored' });
      });
  };

  return (
    <React.Fragment>
      <div className="profile-heading">
        <p className="profile-heading-subcontent">Hello my friend !</p>
        <h2 className="profile-heading-content">
          {user?.firstName} {user?.lastName}
        </h2>
        <Avatar imgSource={user?.avatar} changeAvatar={changeAvatar} alt="Avatar user" />
      </div>
    </React.Fragment>
  );
};

export default ProfileHeading;
