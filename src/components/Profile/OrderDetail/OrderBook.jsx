import React from 'react';

import { ORDER_BOOK_STATUS } from 'src/configs/constant';

import { FcCheckmark as MarkIcon } from 'react-icons/fc';

const OrderBook = ({ orderBook, checkStatus, bookRtn, setBookRtn }) => {
  const [clicked, setClicked] = React.useState(false);

  const setStyle = () => {
    if (checkStatus(orderBook) === ORDER_BOOK_STATUS.RETURNED) {
      return { color: 'green' };
    }
    if (checkStatus(orderBook) === ORDER_BOOK_STATUS.EXPIRED) {
      return { color: 'red' };
    }
    return { color: 'dodgerblue' };
  };

  const chooseBookReturn = () => {
    if (!clicked) {
      setBookRtn([...bookRtn, orderBook.book.id]);
    } else {
      setBookRtn([...bookRtn.filter((item) => item !== orderBook.book.id)]);
    }
  };
  return (
    <React.Fragment>
      <li className="row mb-5">
        <div className="media">
          <img
            className="otherbook-img"
            src={orderBook.book.image}
            alt="This is a book"
          />
          <div className="info">{orderBook.book.title}</div>
          <div className="status" style={setStyle()}>
            <div className="title">{checkStatus(orderBook)}</div>
            <div
              className="tick-box"
              onClick={() => {
                setClicked(!clicked);
                chooseBookReturn();
              }}
            >
              {clicked ? <MarkIcon /> : null}
            </div>
          </div>
        </div>
      </li>
    </React.Fragment>
  );
};

export default OrderBook;
