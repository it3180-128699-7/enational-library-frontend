import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

import ProfileTemplate from 'src/layout/Profile/template';

import { getToken } from 'src/utils/tokenUtil';
import userOrderApi from 'src/features/userOrder/userOrderApi';
import { ORDER_BOOK_STATUS } from 'src/configs/constant';

import OrderBook from './OrderBook';

import './OrderItem.css';
import { toast } from 'react-toastify';

const OrderDetail = () => {
  const params = useParams();

  const [orderDetails, setOrderDetails] = useState([]);
  const [bookRtn, setBookRtn] = useState([]);

  const checkStatus = (orderBook) => {
    if (!orderBook.receivedTime) {
      return ORDER_BOOK_STATUS.PENDING;
    }

    if (orderBook.status === ORDER_BOOK_STATUS.RETURNING) {
      return ORDER_BOOK_STATUS.RETURNING;
    }

    if (orderBook.status === ORDER_BOOK_STATUS.RETURNED) {
      return ORDER_BOOK_STATUS.RETURNED;
    }
    //CALCULATE DAYS LEFT
    const nowTime = new Date();
    const expirationTime = new Date(orderBook.expirationTime);

    if (nowTime > expirationTime) {
      return ORDER_BOOK_STATUS.EXPIRED;
    }
    const diff_time = expirationTime.getTime() - nowTime.getTime();
    const diff_day = diff_time / (1000 * 3600 * 24);

    return `${Math.round(diff_day)} days left`;
  };

  const updateOrder = (orderReturn) => {
    setOrderDetails([
      ...orderDetails.map((item) => {
        const idx = orderReturn.findIndex((val) => val.book.id === item.book.id);
        if (idx !== -1) {
          return { ...orderReturn[idx] };
        } else {
          return { ...item };
        }
      }),
    ]);
  };

  const returnedBook = (bookId) => {
    userOrderApi
      .returnBooks(bookId, getToken())
      .then((response) => {
        toast.success('Return book successful!', { theme: 'colored' });
        updateOrder(response.data);
        setBookRtn([]);
      })
      .catch((error) => {
        console.log(error);
        toast.error(error?.response?.data?.message, { theme: 'colored' });
      });
  };

  useEffect(() => {
    const getOrderDetails = async () => {
      const orderResponse = await userOrderApi.getAnOrder(params.id, getToken());
      setOrderDetails(orderResponse.data.orderBooks);
    };
    getOrderDetails();
  }, [params]);

  return (
    <ProfileTemplate>
      <div className="detail active">
        <div className="title">
          <div className="name">
            <div className="index">Order #{params.id}</div>
            <div className="info">{orderDetails.length} books</div>
          </div>
        </div>

        <ul className="item">
          {orderDetails.map((orderBook, index) => (
            <OrderBook
              key={index}
              orderBook={orderBook}
              checkStatus={checkStatus}
              bookRtn={bookRtn}
              setBookRtn={setBookRtn}
            />
          ))}
        </ul>
        <button
          className="form-submit"
          title={
            orderDetails[0]?.receivedTime
              ? 'Click to book you want to return'
              : 'You have to receive all of this'
          }
          disabled={!orderDetails[0]?.receivedTime}
          onClick={() => returnedBook(bookRtn)}
        >
          Returned books
        </button>
      </div>
    </ProfileTemplate>
  );
};

export default OrderDetail;
