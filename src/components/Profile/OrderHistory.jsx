import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { format as formatTime } from 'date-fns';

import ProfileTemplate from 'src/layout/Profile/template';

import { getToken } from 'src/utils/tokenUtil';
import {
  fetchGetOrders,
  fetchConfirmRcv,
  selectOrderList,
} from 'src/features/userOrder/userOrderSlice';
import { ORDER_BOOK_STATUS } from 'src/configs/constant';

import './OrderHistory.css';
import { toast } from 'react-toastify';

const OrderHistory = () => {
  const dispatch = useDispatch();
  const orders = useSelector(selectOrderList);

  useEffect(() => {
    dispatch(fetchGetOrders(getToken()));
  }, [dispatch]);

  const checkStatus = (orderBooks) => {
    if (!orderBooks[0].receivedTime) {
      return ORDER_BOOK_STATUS.PENDING;
    }

    if (!orderBooks.filter((book) => book.status !== ORDER_BOOK_STATUS.RETURNED).length) {
      return ORDER_BOOK_STATUS.RETURNED;
    }
    //Calculate day lefts
    const nowTime = new Date();
    const expirationTime = new Date(orderBooks[0].expirationTime);
    if (nowTime > expirationTime) {
      return ORDER_BOOK_STATUS.EXPIRED;
    }
    const diff_time = expirationTime.getTime() - nowTime.getTime();
    const diff_day = diff_time / (1000 * 3600 * 24);

    return `${Math.round(diff_day)} days left`;
  };

  const setStyle = (status) => {
    if (status === ORDER_BOOK_STATUS.EXPIRED) {
      return { color: 'red' };
    }
    if (status === ORDER_BOOK_STATUS.RETURNED) {
      return { color: 'green' };
    }

    return { color: 'dodgerblue' };
  };

  const confirmReceive = (orderID) => {
    dispatch(
      fetchConfirmRcv({
        orderID,
        token: getToken(),
      })
    );
    toast.success('Confirm receive successful', { theme: 'colored' });
  };

  return (
    <ProfileTemplate>
      <ul className="list-group list-group-flush order_his-ul">
        {orders.map((order) => (
          <div className="list-group-item order_his-item">
            <div className="order_his-item-left">
              <Link to={`/profile/order-history/${order.id}`}>
                <div className="order_his-lot">OrderID: #{order.id}</div>
              </Link>
              <div className="order_his-count">{order.orderBooks.length} books</div>
              <div className="order_his-date">
                Date ordered:{' '}
                {formatTime(new Date(order.orderBooks[0].rentedTime), 'HH:mm:ss - MMM, dd yyyy')}
              </div>
            </div>

            <div className="order_his-quantity" style={setStyle(checkStatus(order.orderBooks))}>
              {checkStatus(order.orderBooks)}
              <div>
                <button
                  className="order_his-confirm"
                  onClick={() => confirmReceive(order.id)}
                  disabled={!(checkStatus(order.orderBooks) === ORDER_BOOK_STATUS.PENDING)}
                  title={
                    !(checkStatus(order.orderBooks) === ORDER_BOOK_STATUS.PENDING)
                      ? "You've already received this order"
                      : ''
                  }
                >
                  Received
                </button>
              </div>
            </div>
          </div>
        ))}
      </ul>
    </ProfileTemplate>
  );
};

export default OrderHistory;
