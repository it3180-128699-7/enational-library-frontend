import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { toast } from 'react-toastify';

import { getToken } from 'src/utils/tokenUtil';
import { selectUser, changeUserData } from 'src/features/authentication/authenSlice';
import { changeUserDataAPI } from 'src/features/authentication/userAPI';

import ProfileTemplate from 'src/layout/Profile/template';

const schema = yup.object().shape({
  firstNameInfor: yup.string().required('This field is required'),
  lastNameInfor: yup.string().required('This field is required'),
});

const ProfileInformation = () => {
  const dispatch = useDispatch();

  //global state
  const user = useSelector(selectUser);

  //validate form
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  //local state
  const submitForm = (data) => {
    changeUserDataAPI(
      {
        firstName: data.firstNameInfor,
        lastName: data.lastNameInfor,
      },
      getToken()
    )
      .then((response) => {
        if (response) {
          toast.success('Saved information successful!', { theme: 'colored' });
          dispatch(changeUserData(response.data));
        }
      })
      .catch((err) => {
        toast.error(err.response?.data?.message, { theme: 'colored' });
      });
  };

  return (
    <ProfileTemplate>
      <div className="profile-form-group active">
        <div className="label">Email</div>
        <input
          type="email"
          name="email"
          id=""
          value={user?.email}
          className="form-input"
          disabled={true}
        />

        <div className="label">Phone number</div>
        <input
          type="text"
          name="phone"
          id=""
          value={user?.phone}
          className="form-input"
          disabled={true}
        />
      </div>

      <form className="profile-form-group active" onSubmit={handleSubmit(submitForm)}>
        <div className="label">First name</div>
        <input
          type="text"
          id=""
          autoComplete='off'
          defaultValue={user?.firstName}
          className="form-input"
          {...register('firstNameInfor')}
        />
        <div className="error-label">{errors.firstNameInfor?.message}</div>

        <div className="label">Last name</div>
        <input
          type="text"
          id=""
          autoComplete="off"
          defaultValue={user?.lastName}
          className="form-input"
          {...register('lastNameInfor')}
        />
        <div className="error-label">{errors.lastNameInfor?.message}</div>
        <input type="submit" className="profile-form-submit" value="Saved" />
      </form>
    </ProfileTemplate>
  );
};

export default ProfileInformation;
