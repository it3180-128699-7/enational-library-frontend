import React from 'react';
import Cookies from 'js-cookie';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { toast } from 'react-toastify';

import ProfileTemplate from 'src/layout/Profile/template';
import { authenAPI } from 'src/features/authentication/authenAPI';

import './ProfileSecurity.css';

const schema = yup.object().shape({
  oldPassword: yup
    .string()
    .required('This field is required')
    .min(8, 'Your password must be at least 8 characters'),
  newPassword: yup
    .string()
    .required('This field is required')
    .min(8, 'Your password must be at least 8 characters')
    .test('password-match', 'New password must be different from the old one', function (value) {
      return this.parent.oldPassword !== value;
    }),
  confirmPassword: yup
    .string()
    .required('This field is required')
    .min(8, 'Your password must be at least 8 characters')
    .oneOf([yup.ref('newPassword'), null], 'password must matches'),
});

const ProfileSecurity = () => {
  //validate form
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  const submitChangePassword = (data) => {
    authenAPI
      .changePassAPI({
        body: {
          oldPassword: data.oldPassword,
          newPassword: data.newPassword,
        },
        token: Cookies.get('token'),
      })
      .then((response) => {
        if (response) {
          toast.success('Change password successful!', { theme: 'colored' });
        }
      })
      .catch((error) => {
        toast.error(error?.response?.data?.message, { theme: 'colored' });
      });
    // console.log(data);
  };

  return (
    <ProfileTemplate>
      <form className='profile-form-group active' onSubmit={handleSubmit(submitChangePassword)}>
        <div className="label">Enter an old password</div>
        <input type="password" className="form-input" {...register('oldPassword')} />
        <div className="error-label">{errors.oldPassword?.message}</div>

        <div className="label">Enter a new password</div>
        <input type="password" className="form-input" {...register('newPassword')} />
        <div className="error-label">{errors.newPassword?.message}</div>

        <div className="label">Confirm a new password</div>
        <input type="password" className="form-input" {...register('confirmPassword')} />
        <div className="error-label">{errors.confirmPassword?.message}</div>

        <input type="submit" className="profile-form-submit" />
      </form>
    </ProfileTemplate>
  );
};

export default ProfileSecurity;
