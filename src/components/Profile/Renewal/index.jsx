import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { format as formatTime } from 'date-fns';

import ProfileTemplate from 'src/layout/Profile/template';

import userApi from 'src/features/user/userApi';
import { changeUserData, selectUser } from 'src/features/authentication/authenSlice';

import './Renewal.css';
import { toast } from 'react-toastify';
import { getToken } from 'src/utils/tokenUtil';

const schema = yup.object().shape({
  renewalMonths: yup
    .number('Must be number type')
    .typeError('You must specify a number')
    .required('This fields is required')
    .integer('Renewal month must be an integer')
    .test('checkNumber', 'Number field must greater than 0', (number) => number > 0),
});

const ProfileRenewal = () => {
  const dispatch = useDispatch();

  const user = useSelector(selectUser);
  //local state
  const [selected, setSelected] = React.useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  const submitForm = (data) => {
    userApi
      .renewalAccount(
        {
          isVip: selected,
          month: data.renewalMonths,
        },
        getToken()
      )
      .then((response) => {
        toast.success('Renewal successful', { theme: 'colored' });
        dispatch(changeUserData(response.data));
      })
      .catch((err) => {
        toast.error(err?.response?.data?.message || 'Renewal account failed!', {
          theme: 'colored',
        });
      });
  };

  return (
    <ProfileTemplate>
      <div className="profile-form-group active">
        <div className="profile-renewal-label">Your balance</div>

        <div className="profile-renewal-account-detail">
          <div className="profile-renewal-balance">{user?.account.balance} $</div>

          <div className="profile-renewal-information">
            <div className="profile-renewal-title">Expiration time:</div>
            <div className="profile-renewal-title">
              {user
                ? formatTime(new Date(user.account.expirationTime), 'HH:mm - MMM, dd yyyy')
                : ''}
            </div>
          </div>

          <div className="profile-renewal-information">
            <div className="profile-renewal-title">Member:</div>
            <div
              className="profile-renewal-title"
              style={user?.account?.isVip ? { color: 'green' } : {}}
            >
              {user?.account ? (user?.account?.isVip ? 'V.I.P' : 'Normal') : ''}
            </div>
          </div>

          <div className="profile-renewal-information">
            <div className="profile-renewal-title">Status:</div>
            <div
              className="profile-renewal-title"
              style={user?.account.isActive ? { color: 'green' } : { color: 'red' }}
            >
              {user?.account ? (user?.account.isActive ? 'Active' : 'Block') : ''}
            </div>
          </div>
        </div>

        <form className="profile-renewal-option" onSubmit={handleSubmit(submitForm)}>
          <div className="profile-renewal-option-label">Renewal options</div>

          <div className="profile-renewal-information">
            <div className="profile-renewal-title">Months:</div>
            <div className="profile-renewal-input-wrapper">
              <input
                type="number"
                autoComplete="off"
                className="profile-renewal-input"
                {...register('renewalMonths')}
              />
            </div>
          </div>
          <h6 className="profile-renewal-validate-msg">{errors?.renewalMonths?.message}</h6>

          <div className="profile-renewal-information">
            <div className="profile-renewal-title">Upgrade VIP:</div>
            <div className="profile-renewal-input-wrapper">
              <select
                className="profile-renewal-select"
                onChange={(e) => setSelected(e.target.value)}
              >
                <option value="false">None</option>
                <option value="true">Yes</option>
              </select>
            </div>
          </div>

          <input type="submit" className="profile-form-submit" />
        </form>
      </div>
    </ProfileTemplate>
  );
};

export default ProfileRenewal;
