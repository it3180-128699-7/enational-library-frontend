import React from 'react';
import { Link, useLocation } from 'react-router-dom';

const SidebarItem = ({ item }) => {
  const location = useLocation();
  const [active, setActive] = React.useState(false);

  React.useEffect(() => {
    if (location.pathname === item.url) {
      setActive(true);
    }
  }, [location.pathname, item.url]);

  return (
    <React.Fragment>
      <Link to={`${item.url}`}>
        <div className={active ? 'profile-item-name active' : 'profile-item-name'}>
          {item.title}
        </div>
      </Link>
    </React.Fragment>
  );
};

export default SidebarItem;
