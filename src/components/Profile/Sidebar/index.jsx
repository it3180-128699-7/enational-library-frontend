import React from 'react';
import SidebarItem from './SidebarItem';

const ProfileSidebar = () => {
  const items = [
    {
      id: 1,
      title: 'Personal Account',
      url: '/profile/information',
    },
    {
      id: 2,
      title: 'Change Password',
      url: '/profile/security',
    },
    {
      id: 3,
      title: 'Order History',
      url: '/profile/order-history',
    },
    {
      id: 4,
      title: 'Renewal Account',
      url: '/profile/renewal'
    }
  ];

  return (
    <React.Fragment>
      <div className="col-sm-12 col-md-3 profile-item">
        {items.map((item) => (
          <SidebarItem key={item.id} item={item} />
        ))}
      </div>
    </React.Fragment>
  );
};

export default ProfileSidebar;
