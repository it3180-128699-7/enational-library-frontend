//to save constant item
export const navItemList = [
  {
    id: 1,
    title: 'Home',
    linkRef: '/',
    itemHide: false,
  },
  {
    id: 2,
    title: 'Category',
    linkRef: '/category',
    itemHide: false,
  },
  {
    id: 3,
    title: 'Authors',
    linkRef: '/authors',
    itemHide: false,
  },
  {
    id: 4,
    title: 'Shop Cart',
    linkRef: '/cart',
    itemHide: true,
  },
  {
    id: 5,
    title: 'Account',
    linkRef: '/profile',
    itemHide: true,
  },
];

export const USER_ROLE = {
  ADMIN: 'Admin',
  STAFF: 'Staff',
  MEMBER: 'Member',
};

export const ADMIN_ACTION = {
  CREATE: 'Create',
  UPDATE: 'Update',
  VIEW: 'View',
  DELETE: 'Delete',
};

export const ORDER_BOOK_STATUS = {
  PENDING: 'pending',
  RECEIVED: 'received',
  RETURNING: 'returning',
  RETURNED: 'returned',
  EXPIRED: 'expired',
};

export const storeImageAPI = 'https://api.cloudinary.com/v1_1/kiennt2811/image/upload';
