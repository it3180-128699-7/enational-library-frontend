import { axiosMethod, axiosRequest } from 'src/utils/fetchUtil';

class OrderApi {
  constructor() {
    this.apiEndpoint = process.env.REACT_APP_API_ENDPOINT;
    this.orderApiEndpoint = this.apiEndpoint + '/orders';
  }

  getOrdersByAdmin(filter, token) {
    return axiosRequest(this.orderApiEndpoint + '/users', axiosMethod.GET, token, filter, null);
  }

  /**
   *
   * @param {{bookId, orderId}[]} items
   * @param {string} token
   * @returns
   */
  confirmReturned(items, token) {
    return axiosRequest(this.orderApiEndpoint + '/confirm-return', axiosMethod.PUT, token, null, {
      items,
    });
  }
}

export default new OrderApi();
