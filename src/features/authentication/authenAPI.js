import { axiosMethod, axiosRequest } from 'src/utils/fetchUtil';

const apiEndPoint = process.env.REACT_APP_API_ENDPOINT;
const authenEndPoint = apiEndPoint + '/auth';

export const authenAPI = {
  loginAPI: (data) => {
    return axiosRequest(authenEndPoint + '/signin', axiosMethod.POST, null, null, data);
  },

  registerAPI: (data) => {
    return axiosRequest(authenEndPoint + '/signup', axiosMethod.POST, null, null, data);
  },

  changePassAPI: (data) => {
    return axiosRequest(
      'http://18.141.82.142:5000/api/auth/password',
      axiosMethod.PUT,
      data.token,
      null,
      data.body
    );
  },
};
