import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import Cookies from 'js-cookie';

import { authenAPI } from './authenAPI';
import { getUserData } from './userAPI';

export const fetchSignIn = createAsyncThunk('auth/signin', async (params, { rejectWithValue }) => {
  try {
    const response = await authenAPI.loginAPI(params);
    return response.data;
  } catch (error) {
    return rejectWithValue(error?.response?.data?.message || error?.response || error);
  }
});

export const fetchSignUp = createAsyncThunk('auth/signup', async (params, { rejectWithValue }) => {
  try {
    const response = await authenAPI.registerAPI(params);
    return response.data;
  } catch (error) {
    return rejectWithValue(error?.response?.data?.message || error?.response || error);
  }
});

export const fetchUserAPI = createAsyncThunk(
  'auth/getUserData',
  async (params, { rejectWithValue }) => {
    try {
      const response = await getUserData(params);
      return response.data;
    } catch (error) {
      return rejectWithValue(error?.response?.data?.message || error?.response || error);
    }
  }
);

const authenSlice = createSlice({
  name: 'authentication',
  initialState: {
    userData: null,
    isLoggedIn: false,

    isFetchingLogin: false,
    fetchLoginMsg: null,
    isWrongLogin: false,

    isFetchingRegister: false,
    fetchRegisterMsg: null,

    isFetchingGetUserData: false,
    fetchGetUserDataMsg: null,
  },
  reducers: {
    logout(state, action) {
      state.userData = null;
      state.isLoggedIn = false;
      Cookies.remove('token');
    },
    login(state, action) {
      state.userData = action.payload.user;
      state.isLoggedIn = true;
      Cookies.set('token', action.payload.token);
    },
    changeUserData(state, action) {
      state.userData = action.payload;
    },
  },

  extraReducers: (builder) => {
    builder
      .addCase(fetchUserAPI.pending, (state, action) => {
        state.isFetchingGetUserData = true;
        state.fetchGetUserDataMsg = null;
      })
      .addCase(fetchUserAPI.fulfilled, (state, action) => {
        state.isFetchingGetUserData = false;
        state.fetchGetUserDataMsg = null;
        state.userData = action.payload;
        state.isLoggedIn = true;
      })
      .addCase(fetchUserAPI.rejected, (state, action) => {
        state.isFetchingGetUserData = false;
        state.fetchGetUserDataMsg = action.payload;
        state.isLoggedIn = false;
        state.userData = null;
      })
      .addCase(fetchSignIn.pending, (state, action) => {
        state.isFetchingLogin = true;
        state.isLoggedIn = false;
      })
      .addCase(fetchSignIn.fulfilled, (state, action) => {
        state.isFetchingLogin = false;
        Cookies.set('token', action.payload.token);
        state.userData = action.payload.user;
        state.isLoggedIn = true;
      })
      .addCase(fetchSignIn.rejected, (state, action) => {
        state.isFetchingAPI = false;
        state.fetchLoginMsg = action.payload;
        state.isLoggedIn = false;
        state.isWrongLogin = true;
      })
      .addCase(fetchSignUp.pending, (state, action) => {
        state.isFetchingRegister = true;
        state.isLoggedIn = false;
      })
      .addCase(fetchSignUp.fulfilled, (state, action) => {
        state.isFetchingRegister = false;
        state.isLoggedIn = true;
        state.userData = action.payload.user;
        Cookies.set('token', action.payload.token);
      })
      .addCase(fetchSignUp.rejected, (state, action) => {
        state.isLoggedIn = false;
        state.fetchRegisterMsg = action.payload;
      });
  },
});

export const selectUser = (state) => state.auth.userData;
export const selectLoggedInState = (state) => state.auth.isLoggedIn;
export const { logout, login, changeUserData } = authenSlice.actions;

export default authenSlice.reducer;
