import { axiosMethod, axiosRequest } from 'src/utils/fetchUtil';

const apiEndPoint = process.env.REACT_APP_API_ENDPOINT;

const userAPI = apiEndPoint + '/users';

export const getUserData = (token) => {
  const userDataAPI = userAPI + '/me';
  return axiosRequest(userDataAPI, axiosMethod.GET, token, null);
};

export const changeUserDataAPI = ({ firstName, lastName }, token) => {
  const userDataAPI = userAPI + '/me';
  return axiosRequest(userDataAPI, axiosMethod.PUT, token, null, {
    firstName,
    lastName,
  });
};

export const changeUserAvatarAPI = (avatar, token) => {
  const userDataAPI = userAPI + '/avatar';
  return axiosRequest(userDataAPI, axiosMethod.PUT, token, null, avatar);
};
