import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { getUserData } from './userAPI';

const initialState = {
  fetchGetUserDataMsg: '',
  userName: '',
  userData: {},
  isFetchingUserAPI: true,
};

export const fetchUserAPI = createAsyncThunk(
  'user/fetchUser',
  async (params, { rejectWithValue }) => {
    try {
      const response = await getUserData(params);
      return response.data;
    } catch (error) {
      return rejectWithValue(error?.response?.data?.message || error?.response || error);
    }
  }
);

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},

  extraReducers: (builder) => {
    builder
      .addCase(fetchUserAPI.rejected, (state, action) => {
        state.isFetchingUserAPI = false;
        state.fetchGetUserDataMsg = action.payload;
      })
      .addCase(fetchUserAPI.pending, (state, action) => {
        state.isFetchingUserAPI = true;
        state.fetchGetUserDataMsg = '';
        state.userData = {};
      })
      .addCase(fetchUserAPI.fulfilled, (state, action) => {
        state.isFetchingUserAPI = false;
        state.fetchGetUserDataMsg = null;
        state.userName = `${action.payload.firstName} ${action.payload.lastName}`;
        state.userData = action.payload;
      });
  },
});

export default userSlice.reducer;
