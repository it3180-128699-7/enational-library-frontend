import { axiosMethod, axiosRequest } from 'src/utils/fetchUtil';

import _ from 'lodash';

class AuthorApi {
  constructor() {
    this.apiEndpoint = process.env.REACT_APP_API_ENDPOINT;
    this.authorApiEndpoint = this.apiEndpoint + '/authors';
  }

  getAuthors() {
    return axiosRequest(this.authorApiEndpoint);
  }

  getOneAuthor(authorId) {
    return axiosRequest(this.authorApiEndpoint + `/${authorId}`);
  }

  createAuthor(author, token) {
    const cleanedAuthor = _.pick(author, ['name', 'avatar', 'bio']);
    return axiosRequest(this.authorApiEndpoint, axiosMethod.POST, token, null, cleanedAuthor);
  }

  updateAuthor(author, token) {
    const cleanedAuthor = _.pick(author, ['name', 'avatar', 'bio']);
    return axiosRequest(
      this.authorApiEndpoint + `/${author.id}`,
      axiosMethod.PUT,
      token,
      null,
      cleanedAuthor
    );
  }

  deleteAuthor(authorId, token) {
    return axiosRequest(this.authorApiEndpoint + `/${authorId}`, axiosMethod.DELETE, token);
  }
}

export default new AuthorApi();
