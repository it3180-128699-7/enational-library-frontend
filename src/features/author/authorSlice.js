import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import authorAPI from './authorApi';

const initialState = {
  authorList: [],
  isFetchingAuthorAPI: true,
};

export const fetchAllAuthors = createAsyncThunk(
  'author/fetchAuthors',
  async (params, { rejectWithValue }) => {
    try {
      const response = await authorAPI.getAuthors();
      return response.data;
    } catch (error) {
      return rejectWithValue(error?.response?.data?.message || error?.response || error);
    }
  }
);

const authorSlice = createSlice({
  name: 'author',
  initialState,
  reducers: {},

  extraReducers: (builder) => {
    builder
      .addCase(fetchAllAuthors.rejected, (state, action) => {
        state.isFetchingAuthorAPI = false;
        state.authorList = [];
      })
      .addCase(fetchAllAuthors.pending, (state, action) => {
        state.isFetchingAuthorAPI = true;
        state.authorList = [];
      })
      .addCase(fetchAllAuthors.fulfilled, (state, action) => {
        state.isFetchingAuthorAPI = false;
        state.authorList = action.payload;
      });
  },
});

export const selectAuthors = (state) => state.author.authorList;

export default authorSlice.reducer;
