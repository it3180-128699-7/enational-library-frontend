import { axiosMethod, axiosRequest } from 'src/utils/fetchUtil';
import _ from 'lodash';

class BookApi {
  constructor() {
    this.apiEndpoint = process.env.REACT_APP_API_ENDPOINT;
    this.bookApiEndpoint = this.apiEndpoint + '/books';
  }

  getBooks(filter, token) {
    return axiosRequest(this.bookApiEndpoint, axiosMethod.GET, token, filter);
  }

  getOneBook(bookId, token) {
    return axiosRequest(this.bookApiEndpoint + `/${bookId}`, axiosMethod.GET, token);
  }

  createBook(book, token) {
    const cleanedBook = _.pick(book, [
      'sku',
      'title',
      'image',
      'description',
      'content',
      'category',
      'author',
      'pageNumber',
      'isPublished',
      'publishedDate',
      'publisher',
      'isExclusive',
    ]);
    return axiosRequest(this.bookApiEndpoint, axiosMethod.POST, token, null, cleanedBook);
  }

  updateBook(book, token) {
    const cleanedBook = _.pick(book, [
      'sku',
      'title',
      'image',
      'description',
      'content',
      'category',
      'author',
      'pageNumber',
      'isPublished',
      'publishedDate',
      'publisher',
      'isExclusive',
    ]);
    return axiosRequest(
      this.bookApiEndpoint + `/${book.id}`,
      axiosMethod.PUT,
      token,
      null,
      cleanedBook
    );
  }

  deleteBook(bookId, token) {
    return axiosRequest(this.bookApiEndpoint + `/${bookId}`, axiosMethod.DELETE, token);
  }
}

export default new BookApi();
