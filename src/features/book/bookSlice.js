import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import bookApi from './bookApi';

const initialState = {
  count: 0,
  books: [],
  isFetchingGetBooks: false,
};

export const fetchGetBooks = createAsyncThunk(
  'book/fetchGetBooks',
  async (params, { rejectWithValue }) => {
    try {
      const response = await bookApi.getBooks(params);
      return response.data;
    } catch (error) {
      return rejectWithValue(error?.response?.data?.message || error?.response || error);
    }
  }
);

export const bookSlice = createSlice({
  name: 'book',
  initialState,
  reducers: {
    cancelList(state, action) {
      state.count = 0;
      state.books = [];
      state.isFetchingGetBooks = true;
    },
  },

  extraReducers: (builder) => {
    builder
      // Handle get books
      .addCase(fetchGetBooks.rejected, (state, action) => {
        state.isFetchingGetBooks = false;
        state.count = 0;
        state.books = [];
      })
      .addCase(fetchGetBooks.pending, (state) => {
        state.isFetchingGetBooks = true;
        state.count = 0;
        state.books = [];
      })
      .addCase(fetchGetBooks.fulfilled, (state, action) => {
        state.isFetchingGetBooks = false;
        state.count = action.payload.meta.totalItems;
        state.books = action.payload.items;
      });
  },
});

export const selectBooksCount = (state) => state.book.count;
export const selectBooks = (state) => state.book.books;
export const selectBookAPIState = (state) => state.book.isFetchingGetBooks;

export const { cancelList } = bookSlice.actions;

export default bookSlice.reducer;
