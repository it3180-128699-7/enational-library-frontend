import { axiosMethod, axiosRequest } from 'src/utils/fetchUtil';

class CartApi {
  constructor() {
    this.apiEndpoint = process.env.REACT_APP_API_ENDPOINT;
    this.bookApiEndpoint = this.apiEndpoint + '/books';
  }

  getBook(id) {
    return axiosRequest(this.bookApiEndpoint + `/${id}`, axiosMethod.GET);
  }
}

export default new CartApi();
