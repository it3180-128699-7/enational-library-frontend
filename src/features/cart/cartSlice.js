import { createSlice } from '@reduxjs/toolkit';

const initialState = [...JSON.parse(localStorage.getItem('cart') || '[]')];

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    deleteItem: (state, action) => {
      let itemList = state;
      itemList.splice(
        itemList.map((it) => JSON.stringify(it)).indexOf(JSON.stringify(action.payload)),
        1
      );
      localStorage.setItem('cart', JSON.stringify(itemList));
      return itemList;
    },
    addtoCart: (state, action) => {
      return [...JSON.parse(localStorage.getItem('cart') || '[]')];
    },
    submitOrderCart: (state, action) => {
      return [];
    },
  },
});

export const { deleteItem, addtoCart, submitOrderCart } = cartSlice.actions;

export default cartSlice.reducer;
