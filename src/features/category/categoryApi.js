import { axiosMethod, axiosRequest } from 'src/utils/fetchUtil';

import _ from 'lodash';

class CategoryApi {
  constructor() {
    this.apiEndpoint = process.env.REACT_APP_API_ENDPOINT;
    this.categoryApiEndpoint = this.apiEndpoint + '/categories';
  }

  getCategories() {
    return axiosRequest(this.categoryApiEndpoint);
  }

  getOneCategory(categoryId) {
    return axiosRequest(this.categoryApiEndpoint + `/${categoryId}`);
  }

  createCategory(category, token) {
    const cleanCategory = _.pick(category, ['title', 'description']);
    return axiosRequest(this.categoryApiEndpoint, axiosMethod.POST, token, null, cleanCategory);
  }

  updateCategory(category, token) {
    const cleanCategory = _.pick(category, ['title', 'description']);
    return axiosRequest(
      this.categoryApiEndpoint + `/${category.id}`,
      axiosMethod.PUT,
      token,
      null,
      cleanCategory
    );
  }

  deleteCategory(categoryId, token) {
    return axiosRequest(this.categoryApiEndpoint + `/${categoryId}`, axiosMethod.DELETE, token);
  }
}

export default new CategoryApi();
