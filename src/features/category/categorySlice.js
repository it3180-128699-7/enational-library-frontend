import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import categoryAPI from './categoryApi';

const initialState = {
  categoryList: [],
  isFetchingGetCat: false,
};

export const fetchAllCategories = createAsyncThunk(
  'category/fetchGetCategories',
  async (params, { rejectWithValue }) => {
    try {
      const response = await categoryAPI.getCategories();
      return response.data;
    } catch (error) {
      return rejectWithValue(error?.response?.data?.message || error?.response || error);
    }
  }
);

const categorySlice = createSlice({
  name: 'categories',
  initialState,
  reducers: {},

  extraReducers: (builder) => {
    builder
      .addCase(fetchAllCategories.rejected, (state, action) => {
        state.isFetchingGetCat = false;
        state.categoryList = [];
      })
      .addCase(fetchAllCategories.pending, (state, action) => {
        state.isFetchingGetCat = true;
        state.categoryList = [];
      })
      .addCase(fetchAllCategories.fulfilled, (state, action) => {
        state.isFetchingGetCat = false;
        state.categoryList = action.payload;
      });
  },
});
export const selectCategories = (state) => state.category.categoryList;

export default categorySlice.reducer;
