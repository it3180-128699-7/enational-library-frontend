import { axiosRequest, axiosMethod } from 'src/utils/fetchUtil';

const apiEndPoint = process.env.REACT_APP_API_ENDPOINT;
const feedbackAPI = apiEndPoint + '/feedbacks';

export const createFeedback = (params) => {
  return axiosRequest(feedbackAPI, axiosMethod.POST, null, null, params);
};

export const getFeedbacks = (params) => {
  return axiosRequest(feedbackAPI, axiosMethod.GET, params, null);
};
