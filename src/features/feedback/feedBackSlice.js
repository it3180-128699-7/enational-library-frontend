import { createAsyncThunk, createSlice, combineReducers } from '@reduxjs/toolkit';
import { createFeedback, getFeedbacks } from './feedBackApi';

export const fetchGetFeedBacks = createAsyncThunk(
  'order/getFeedBacks',
  async (params, { rejectWithValue }) => {
    try {
      const response = await getFeedbacks(params);
      return response.data;
    } catch (error) {
      return rejectWithValue(error?.response?.data?.message || error?.response || error);
    }
  }
);

export const fetchPostFeedBack = createAsyncThunk(
  'order/postFeedBacks',
  async (params, { rejectWithValue }) => {
    try {
      await createFeedback(params);
    } catch (error) {
      return rejectWithValue(error?.response?.data?.message || error?.response || error);
    }
  }
);

const postFeedBack = createSlice({
  name: 'postFeedBack',
  initialState: {
    statusCode: 0,
    isFetchingAPI: true,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchPostFeedBack.rejected, (state, action) => {
        state.statusCode = 401;
        state.isFetchingAPI = false;
      })
      .addCase(fetchPostFeedBack.pending, (state, action) => {
        state.statusCode = 0;
        state.isFetchingAPI = true;
      })
      .addCase(fetchPostFeedBack.fulfilled, (state, action) => {
        state.statusCode = 200;
        state.isFetchingAPI = false;
      });
  },
});

const getFeedBacks = createSlice({
  name: 'getFeedBack',
  initialState: {
    feedbackList: [],
    isFetchingAPI: true,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchGetFeedBacks.rejected, (state, action) => {
        state.isFetchingAPI = false;
        state.feedbackList = [];
      })
      .addCase(fetchGetFeedBacks.pending, (state, action) => {
        state.isFetchingAPI = true;
        state.feedbackList = [];
      })
      .addCase(fetchGetFeedBacks.fulfilled, (state, action) => {
        state.isFetchingAPI = false;
        state.feedbackList = action.payload;
      });
  },
});

export default combineReducers({
  postFeedBack: postFeedBack.reducer,
  getFeedbacks: getFeedBacks.reducer,
});
