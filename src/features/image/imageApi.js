import { axiosRequest, axiosMethod } from 'src/utils/fetchUtil';
import FormData from 'form-data';

class ImageApi {
  constructor() {
    this.apiEndpoint = process.env.REACT_APP_API_ENDPOINT;
    this.imageApiEndpoint = this.apiEndpoint + '/images';
  }

  upload({ image, folder }, token) {
    const data = new FormData();
    data.append('image', image);
    data.append('folder', folder);

    return axiosRequest(this.imageApiEndpoint, axiosMethod.POST, token, null, data);
  }
}

export default new ImageApi();
