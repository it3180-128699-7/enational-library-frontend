import { axiosMethod, axiosRequest } from 'src/utils/fetchUtil';

class PaymentApi {
  constructor() {
    this.apiEndpoint = process.env.REACT_APP_API_ENDPOINT;
    this.bookApiEndpoint = this.apiEndpoint + '/orders';
  }

  createOrder(token, data) {
    return axiosRequest(this.bookApiEndpoint, axiosMethod.POST, token, null, data);
  }
}

export default new PaymentApi();
