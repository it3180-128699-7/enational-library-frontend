import { axiosRequest, axiosMethod } from 'src/utils/fetchUtil';
import { storeImageAPI } from 'src/configs/constant';

class AvatarApi {
  constructor() {
    this.apiEndpoint = storeImageAPI;
  }

  changeAvatarApi(data) {
    return axiosRequest(this.apiEndpoint, axiosMethod.POST, null, null, data);
  }
}

export default new AvatarApi();
