import { axiosMethod, axiosRequest } from 'src/utils/fetchUtil';

class UserApi {
  constructor() {
    this.apiEndpoint = process.env.REACT_APP_API_ENDPOINT;
    this.userApiEndpoint = this.apiEndpoint + '/users';
    this.accountApiEndpoint = this.apiEndpoint + '/accounts';
  }

  getUsers(filter, token) {
    return axiosRequest(this.userApiEndpoint, axiosMethod.GET, token, filter, null);
  }

  updateRole({ userId, role }, token) {
    return axiosRequest(this.userApiEndpoint + '/role', axiosMethod.PUT, token, null, {
      userId,
      role,
    });
  }

  depositMoney({ userId, money }, token) {
    return axiosRequest(this.accountApiEndpoint + '/deposit', axiosMethod.PUT, token, null, {
      userId,
      money,
    });
  }

  updateStatus({ userId, isActive }, token) {
    return axiosRequest(this.userApiEndpoint + '/block', axiosMethod.PUT, token, null, {
      userId,
      isActive,
    });
  }

  renewalAccount({ isVip, month }, token) {
    return axiosRequest(this.accountApiEndpoint + '/renewal', axiosMethod.PUT, token, null, {
      isVip,
      month,
    });
  }
}

export default new UserApi();
