import { axiosMethod, axiosRequest } from 'src/utils/fetchUtil';

class userOrderAPI {
  constructor() {
    this.apiEndPoint = process.env.REACT_APP_API_ENDPOINT;
    this.userOrderApiEndPoint = this.apiEndPoint + '/orders';
  }

  createNewOrder(data, token) {
    return axiosRequest(this.userOrderApiEndPoint, axiosMethod.POST, token, null, data);
  }

  getAllOrder(token) {
    return axiosRequest(this.userOrderApiEndPoint, axiosMethod.GET, token, null, null);
  }

  getAnOrder(orderID, token) {
    return axiosRequest(
      this.userOrderApiEndPoint + `/${orderID}`,
      axiosMethod.GET,
      token,
      null,
      null
    );
  }

  confirmReceiveOrder(orderID, token) {
    return axiosRequest(
      this.userOrderApiEndPoint + `/confirm-receive/${orderID}`,
      axiosMethod.PUT,
      token,
      null,
      null
    );
  }

  returnBooks(bookId, token) {
    return axiosRequest(this.userOrderApiEndPoint + '/return', axiosMethod.PUT, token, null, {
      bookId,
    });
  }
}

export default new userOrderAPI();
