import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import userOrderApi from 'src/features/userOrder/userOrderApi';

export const fetchGetOrders = createAsyncThunk(
  'user/getAllOrders',
  async (token, { rejectWithValue }) => {
    try {
      const response = await userOrderApi.getAllOrder(token);
      return response.data;
    } catch (error) {
      return rejectWithValue(error?.response?.data?.message || error?.response || error);
    }
  }
);

export const fetchConfirmRcv = createAsyncThunk(
  'user/confirmReceive',
  async (params, { rejectWithValue }) => {
    try {
      const response = await userOrderApi.confirmReceiveOrder(params.orderID, params.token);
      return response.data;
    } catch (error) {
      return rejectWithValue(error?.response?.data?.message || error?.response || error);
    }
  }
);

export const fetchGetAnOrder = createAsyncThunk(
  'user/getAnOrder',
  async (params, { rejectWithValue }) => {
    try {
      const response = await userOrderApi.getAnOrder(params.orderID, params.token);
      return response.data;
    } catch (error) {
      return rejectWithValue(error?.response?.data?.message || error?.response || error);
    }
  }
);

const orderSlice = createSlice({
  name: 'getUserOrder',
  initialState: {
    orderList: [],
    orderBooks: [],

    isFetchingGetOrdersAPI: false,
    fetchGetOrdersAPIMsg: null,

    isFetchingGetAnOrderAPI: false,
    fetchGetAnOrderAPIMsg: null,

    isFetchingConfirmRcv: false,
    fetchConfirmRcvMsg: null,

    isFetchingRtnBooks: false,
    fetchRtnBooksMsg: null,
  },
  reducers: {},

  extraReducers: (builder) => {
    builder
      .addCase(fetchGetOrders.pending, (state, action) => {
        state.isFetchingGetOrdersAPI = true;
      })
      .addCase(fetchGetOrders.fulfilled, (state, action) => {
        state.isFetchingGetOrdersAPI = false;
        state.orderList = action.payload;
      })
      .addCase(fetchGetOrders.rejected, (state, action) => {
        state.isFetchingOrderAPI = false;
        state.fetchGetOrdersAPIMsg = action.payload;
        state.orderList = null;
      })
      .addCase(fetchGetAnOrder.pending, (state, action) => {
        state.orderBooks = [];
        state.isFetchingGetAnOrderAPI = true;
      })
      .addCase(fetchGetAnOrder.fulfilled, (state, action) => {
        state.isFetchingGetAnOrderAPI = false;
        state.orderBooks = action.payload.orderBooks;
      })
      .addCase(fetchGetAnOrder.rejected, (state, action) => {
        state.isFetchingGetAnOrderAPI = false;
        state.fetchGetAnOrderAPIMsg = action.payload;
        state.orderBooks = null;
      })
      .addCase(fetchConfirmRcv.pending, (state, action) => {
        state.isFetchingConfirm = true;
      })
      .addCase(fetchConfirmRcv.fulfilled, (state, action) => {
        state.isFetchingConfirm = false;
        state.orderList = [
          ...state.orderList.map((order) => {
            if (order.id === action.payload.id) {
              return { ...action.payload };
            } else {
              return { ...order };
            }
          }),
        ];
      })
      .addCase(fetchConfirmRcv.rejected, (state, action) => {
        state.isFetchingConfirm = false;
        state.fetchConfirmRcvMsg = action.payload;
      })
  },
});

export const selectOrderList = (state) => state.order.orderList;
export const selectAnOrder = (state) => state.order.orderBooks;

export default orderSlice.reducer;
