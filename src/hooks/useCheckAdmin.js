import { useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router';

import { USER_ROLE } from 'src/configs/constant';
import useCheckLogin from 'src/hooks/useCheckLogin';

export default function useCheckAdmin() {
  const location = useLocation();
  const navigate = useNavigate();
  const { userData, isLoggedIn, isFetching } = useCheckLogin({ currentPage: location.pathname });

  useEffect(() => {
    if (isLoggedIn && !isFetching && !!userData) {
      if (userData.role !== USER_ROLE.ADMIN && userData.role !== USER_ROLE.STAFF) {
        navigate('/auth/login');
      } else {
        navigate(location.pathname);
      }
    }
  }, [isFetching, isLoggedIn, navigate, userData, location.pathname]);
}
