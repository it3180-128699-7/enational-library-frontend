import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { useNavigate } from 'react-router-dom';

import { getToken } from 'src/utils/tokenUtil';

import {
  fetchUserAPI,
  selectLoggedInState,
  selectUser,
} from 'src/features/authentication/authenSlice';

export default function useCheckLogin({ currentPage }) {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const isLoggedIn = useSelector(selectLoggedInState);
  const userData = useSelector(selectUser);
  const isFetching = useSelector((state) => state.auth.isFetchingUserData);

  useEffect(() => {
    return dispatch(fetchUserAPI(getToken()));
  }, [dispatch]);

  useEffect(() => {
    //reload page
    if (!getToken()) {
      navigate('/auth/login', { state: { lastUrl: currentPage } });
    }
    //log out
    if (!getToken() && !isLoggedIn && !isFetching) {
      navigate('/auth/login', { state: { lastUrl: currentPage } });
    }
  }, [currentPage, isLoggedIn, isFetching, userData, navigate]);

  return { isLoggedIn, userData, isFetching };
}
