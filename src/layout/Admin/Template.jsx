import React from 'react';
import Helmet from 'react-helmet';

import AdminSidebar from 'src/components/Admin/Sidebar';
import AdminNavbar from 'src/components/Admin/Navbar';

import './template.css';
import useCheckAdmin from 'src/hooks/useCheckAdmin';

const AdminPageTemplate = ({ title, children }) => {
  useCheckAdmin();

  return (
    <>
      <React.Fragment>
        <Helmet>
          <title>{`ENational Library | CMS | ${title}`}</title>
        </Helmet>

        <div className="adminpage-wrapper">
          <AdminSidebar />
          <div className="adminpage-main-panel">
            <AdminNavbar title={title} />
            <div className="adminpage-content p-5">{children}</div>
          </div>
        </div>
      </React.Fragment>
    </>
  );
};

export default AdminPageTemplate;
