import React from 'react';
import { Helmet } from 'react-helmet';

import Header from 'src/components/Header';
import Footer from 'src/components/Footer';
import ProfileHeading from 'src/components/Profile/Heading';
import ProfileSidebar from 'src/components/Profile/Sidebar';

const ProfileTemplate = ({ children }) => {
  return (
    <React.Fragment>
      <Helmet>
        <title>Profile | ENational Library</title>
      </Helmet>
      <Header />
      <div className="container">
        <ProfileHeading />
        <div className="row profile-main">
          <ProfileSidebar />
          <div className="col-sm-12 col-md-6 profile-content">
            <div className="profile-pane active">{children}</div>
          </div>
        </div>
      </div>
      <Footer />
    </React.Fragment>
  );
};

export default ProfileTemplate;
