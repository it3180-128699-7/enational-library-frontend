import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';

import AdminPageTemplate from 'src/layout/Admin/Template';
import NoData from 'src/components/NoData';
import authorApi from 'src/features/author/authorApi';
import AuthorsTable from 'src/components/Admin/Author/AuthorsTable';

const AdminAuthorPage = () => {
  const [authors, setAuthors] = useState([]);

  useEffect(() => {
    async function fetchAuthors() {
      const authorsResponse = await authorApi.getAuthors();
      setAuthors(authorsResponse.data);
    }

    fetchAuthors();
  }, []);

  return (
    <AdminPageTemplate title={'Author'}>
      <Container fluid>
        <Row>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title>Author Management</Card.Title>
              </Card.Header>

              <Card.Body>
                {authors.length ? (
                  <AuthorsTable authors={authors} setAuthors={setAuthors} />
                ) : (
                  <div className="d-flex justify-content-center">
                    <NoData height={500} width={500} />
                  </div>
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </AdminPageTemplate>
  );
};

export default AdminAuthorPage;
