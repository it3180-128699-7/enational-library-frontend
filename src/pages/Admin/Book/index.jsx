import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';

import AdminPageTemplate from 'src/layout/Admin/Template';
import Pagination from 'src/components/Pagination';
import NoData from 'src/components/NoData';
import bookApi from 'src/features/book/bookApi';
import categoryApi from 'src/features/category/categoryApi';
import authorApi from 'src/features/author/authorApi';
import { getToken } from 'src/utils/tokenUtil';
import BookToolbar from 'src/components/Admin/Book/BookToolbar';
import BooksTable from 'src/components/Admin/Book/BooksTable';

const AdminBookPage = () => {
  const [filter, setFilter] = useState({
    page: 1,
    limit: 10,
    search: '',
    sort: 0,
    exclusive: 0,
    category: 0,
    author: 0,
  });
  const [filterMeta, setFilterMeta] = useState({
    itemCount: 0,
    totalItems: 0,
    itemsPerPage: 10,
    totalPages: 0,
    currentPage: 1,
  });
  const [books, setBooks] = useState([]);
  const [authors, setAuthors] = useState([]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    async function fetchBooks() {
      const booksResponse = await bookApi.getBooks(filter, getToken());
      const categoriesResponse = await categoryApi.getCategories();
      const authorsResponse = await authorApi.getAuthors();
      setFilterMeta(booksResponse.data.meta);
      setBooks(booksResponse.data.items);
      setCategories(categoriesResponse.data);
      setAuthors(authorsResponse.data);
    }

    fetchBooks();
  }, [filter]);

  return (
    <AdminPageTemplate title={'Book'}>
      <Container fluid>
        <Row>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title>Book Management</Card.Title>
              </Card.Header>

              <Card.Body>
                {categories.length && authors.length ? (
                  <BookToolbar
                    filter={filter}
                    setFilter={setFilter}
                    categories={categories}
                    authors={authors}
                  />
                ) : null}

                {books.length ? (
                  <BooksTable
                    books={books}
                    setBooks={setBooks}
                    categories={categories}
                    authors={authors}
                  />
                ) : (
                  <div className="d-flex justify-content-center">
                    <NoData height={500} width={500} />
                  </div>
                )}
              </Card.Body>
              <Card.Footer>
                <div className="d-flex justify-content-end">
                  <Pagination
                    pagesCount={filterMeta.totalPages}
                    page={filter.page}
                    setPage={(page) => setFilter({ ...filter, page })}
                  />
                </div>
              </Card.Footer>
            </Card>
          </Col>
        </Row>
      </Container>
    </AdminPageTemplate>
  );
};

export default AdminBookPage;
