import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';

import AdminPageTemplate from 'src/layout/Admin/Template';
import NoData from 'src/components/NoData';
import categoryApi from 'src/features/category/categoryApi';
import CategoriesTable from 'src/components/Admin/Category/CategoriesTable';

const AdminCategoryPage = () => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    async function fetchCategories() {
      const categoriesResponse = await categoryApi.getCategories();
      setCategories(categoriesResponse.data);
    }

    fetchCategories();
  }, []);

  return (
    <AdminPageTemplate title={'Category'}>
      <Container fluid>
        <Row>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title>Category Management</Card.Title>
              </Card.Header>

              <Card.Body>
                {categories.length ? (
                  <CategoriesTable categories={categories} setCategories={setCategories} />
                ) : (
                  <div className="d-flex justify-content-center">
                    <NoData height={500} width={500} />
                  </div>
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </AdminPageTemplate>
  );
};

export default AdminCategoryPage;
