import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';

import orderApi from 'src/features/admin-order/orderApi';
import { getToken } from 'src/utils/tokenUtil';
import AdminPageTemplate from 'src/layout/Admin/Template';
import OrdersTable from 'src/components/Admin/Order/OrderTable.jsx';
import OrderToolBar from 'src/components/Admin/Order/OrderToolBar';
import Pagination from 'src/components/Pagination';
import NoData from 'src/components/NoData';

const AdminOrderPage = () => {
  const [filter, setFilter] = useState({
    page: 1,
    limit: 10,
    search: '',
    sort: 0,
    status: '',
  });
  const [filterMeta, setFilterMeta] = useState({
    itemCount: 0,
    totalItems: 0,
    itemsPerPage: 10,
    totalPages: 0,
    currentPage: 1,
  });
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    async function fetchOrders() {
      const ordersResponse = await orderApi.getOrdersByAdmin(filter, getToken());
      setFilterMeta(ordersResponse.data.meta);
      setOrders(ordersResponse.data.items);
    }
    fetchOrders();
  }, [filter]);

  return (
    <AdminPageTemplate title={'Order'}>
      <Container fluid>
        <Row>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title>Order management</Card.Title>
              </Card.Header>
              <Card.Body>
                <OrderToolBar filter={filter} setFilter={setFilter}></OrderToolBar>
                {orders.length ? (
                  <OrdersTable orders={orders} setOrders={setOrders} />
                ) : (
                  <div className="d-flex justify-content-center">
                    <NoData height={500} width={500} />
                  </div>
                )}
              </Card.Body>
              <Card.Footer>
                <div className="d-flex justify-content-end">
                  <Pagination
                    pagesCount={filterMeta.totalPages}
                    page={filter.page}
                    setPage={(page) => setFilter({ ...filter, page })}
                  />
                </div>
              </Card.Footer>
            </Card>
          </Col>
        </Row>
      </Container>
    </AdminPageTemplate>
  );
};

export default AdminOrderPage;
