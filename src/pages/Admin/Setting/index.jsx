import React from 'react';

import AdminPageTemplate from 'src/layout/Admin/Template';
import Maintenance from 'src/components/Maintenance';

const AdminSettingPage = () => {
  return (
    <AdminPageTemplate title={'Setting'}>
      <div className="d-flex flex-column justify-content-center align-items-center">
        <Maintenance width={500} height={500} />
        <div className="fs-3">This feature is under mantainence</div>
      </div>
    </AdminPageTemplate>
  );
};

export default AdminSettingPage;
