import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';

import AdminPageTemplate from 'src/layout/Admin/Template';
import Pagination from 'src/components/Pagination';
import NoData from 'src/components/NoData';
import userApi from 'src/features/user/userApi';
import { getToken } from 'src/utils/tokenUtil';
import UsersTable from 'src/components/Admin/User/UserTable';
import UserToolbar from 'src/components/Admin/User/UserToolbar';

const AdminUserPage = () => {
  const [filter, setFilter] = useState({
    page: 1,
    limit: 10,
    search: '',
    sort: 0,
    role: 0,
    isActive: 0,
    isVip: 0,
    isExpired: 0,
    foulTimes: 0,
  });
  const [filterMeta, setFilterMeta] = useState({
    itemCount: 0,
    totalItems: 0,
    itemsPerPage: 10,
    totalPages: 0,
    currentPage: 1,
  });
  const [users, setUsers] = useState([]);

  useEffect(() => {
    async function fetchUsers() {
      const usersResponse = await userApi.getUsers(filter, getToken());
      setFilterMeta(usersResponse.data.meta);
      setUsers(usersResponse.data.items);
    }

    fetchUsers();
  }, [filter]);

  return (
    <AdminPageTemplate title={'User'}>
      <Container fluid>
        <Row>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title>User management</Card.Title>
              </Card.Header>

              <Card.Body>
                <UserToolbar filter={filter} setFilter={setFilter} />
                {users.length ? (
                  <UsersTable users={users} setUsers={setUsers} />
                ) : (
                  <div className="d-flex justify-content-center">
                    <NoData height={500} width={500} />
                  </div>
                )}
              </Card.Body>
              <Card.Footer>
                <div className="d-flex justify-content-end">
                  <Pagination
                    pagesCount={filterMeta.totalPages}
                    page={filter.page}
                    setPage={(page) => setFilter({ ...filter, page })}
                  />
                </div>
              </Card.Footer>
            </Card>
          </Col>
        </Row>
      </Container>
    </AdminPageTemplate>
  );
};

export default AdminUserPage;
