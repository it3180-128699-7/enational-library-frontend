import React from 'react';
import { Helmet } from 'react-helmet';

import Login from 'src/components/Auth/Login';

import './LoginPage.css';

const LoginPage = () => {
  return (
    <React.Fragment>
      <Helmet>
        <title>Login | ENational Library</title>
      </Helmet>
      <div className="login-background">
        <Login />
      </div>
    </React.Fragment>
  );
};

export default LoginPage;
