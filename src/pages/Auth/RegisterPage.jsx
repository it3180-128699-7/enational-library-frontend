import React from 'react';
import { Helmet } from 'react-helmet';

import Register from 'src/components/Auth/Register';

const RegisterPage = () => {
  return (
    <>
      <Helmet>
        <title>Register | ENational Library</title>
      </Helmet>
      <div className="login-background">
        <Register />
      </div>
    </>
  );
};

export default RegisterPage;
