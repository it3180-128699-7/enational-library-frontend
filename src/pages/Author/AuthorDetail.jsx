import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router';

import { getToken } from 'src/utils/tokenUtil';
import authorApi from 'src/features/author/authorApi';
import bookApi from 'src/features/book/bookApi';
import Header from 'src/components/Header';
import Footer from 'src/components/Footer';
import AuthorInfo from 'src/components/Author/AuthorInfo';
import BookList from 'src/components/Book/BookList';
import './style.css';
import NoData from 'src/components/NoData';

const AuthorDetailPage = () => {
  const params = useParams();

  const [author, setAuthor] = useState(null);
  const [books, setBooks] = useState([]);

  useEffect(() => {
    const fetchApi = async () => {
      const authorResponse = await authorApi.getOneAuthor(params.id);
      const booksResponse = await bookApi.getBooks({ author: params.id }, getToken());
      setAuthor(authorResponse.data);
      setBooks(booksResponse.data.items);
    };

    fetchApi();
  }, [params.id]);

  return (
    <React.Fragment>
      <Helmet>
        <title>Enational Library | Author</title>
      </Helmet>
      <div id="main">
        <Header />
        {!!author && !!books && (
          <div className="container pt-5">
            <div className="row mb-5">
              <div className="col">
                <AuthorInfo author={author} />
              </div>
            </div>
            <div className="row mb-3">
              <div className="fs-4" style={{ fontWeight: 500 }}>
                Books of {author.name}
              </div>
            </div>
            <div className="row mb-5">
              <div className="col">
                {books?.length ? (
                  <BookList books={books} />
                ) : (
                  <div className="d-flex justify-content-center">
                    <NoData width={300} height={300} />
                  </div>
                )}
              </div>
            </div>
          </div>
        )}
        <Footer />
      </div>
    </React.Fragment>
  );
};

export default AuthorDetailPage;
