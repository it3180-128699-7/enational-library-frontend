import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';

import authorApi from 'src/features/author/authorApi';
import Header from 'src/components/Header';
import Footer from 'src/components/Footer';
import AuthorList from 'src/components/Author/AuthorList';
import './style.css';

const AuthorPage = () => {
  const [authors, setAuthors] = useState([]);

  useEffect(() => {
    const fetchApi = async () => {
      const authorResponse = await authorApi.getAuthors();
      setAuthors(authorResponse.data);
    };

    fetchApi();
  }, []);

  return (
    <React.Fragment>
      <Helmet>
        <title>Enational Library | Authors</title>
      </Helmet>
      <div id="main">
        <Header />
        <div className="container pt-5 authors-wrapper">
          <div className="row pb-3">
            <div className="col d-flex justify-content-center">
              <h2 className="authors--heading__h2">All authors</h2>
            </div>
          </div>
          <div className="row">
            <AuthorList authors={authors} />
          </div>
        </div>
        <Footer />
      </div>
    </React.Fragment>
  );
};

export default AuthorPage;
