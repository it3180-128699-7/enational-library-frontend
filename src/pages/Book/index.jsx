import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { Container } from 'react-bootstrap';

import './BookPage.css';
import { fetchGetBooks, selectBooks, selectBooksCount } from 'src/features/book/bookSlice';
import BooksGrid from 'src/components/Book/BooksGrid';

const BookPage = () => {
  const dispatch = useDispatch();

  // global state
  const booksCount = useSelector(selectBooksCount);
  const books = useSelector(selectBooks);

  // local state
  const [filter, setFilter] = useState({
    page: 1,
    limit: 8,
  });

  useEffect(() => {
    dispatch(fetchGetBooks());
  }, [dispatch]);

  return (
    <>
      <Helmet>
        <title>Book | ENational Library</title>
      </Helmet>
      <Container>
        <h1>Books</h1>
        {!!books && (
          <BooksGrid count={booksCount} books={books} filter={filter} setFilter={setFilter} />
        )}
      </Container>
    </>
  );
};

export default BookPage;
