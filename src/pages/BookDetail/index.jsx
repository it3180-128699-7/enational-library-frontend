import React from 'react';
import { Helmet } from 'react-helmet';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router';

import { getToken } from 'src/utils/tokenUtil';
import bookApi from 'src/features/book/bookApi';
import Header from 'src/components/Header';
import Footer from 'src/components/Footer';
import BookDetail from 'src/components/BookDetail/BookDetail';
import NoData from 'src/components/NoData';

const BookDetailPage = () => {
  let params = useParams();
  const [book, setBook] = useState(null);
  useEffect(() => {
    async function fetchBookDetail() {
      const usersResponse = await bookApi.getOneBook(params.id, getToken());
      setBook(usersResponse.data);
    }

    fetchBookDetail();
  }, [params.id]);

  return (
    <React.Fragment>
      <Helmet>
        <title>Book Detail | ENational Library</title>
      </Helmet>
      <div id="main">
        <Header />
        {!!book ? (
          <BookDetail book={book} />
        ) : (
          <div className="d-flex justify-content-center">
            <NoData height={500} width={500} />
          </div>
        )}

        <Footer />
      </div>
    </React.Fragment>
  );
};

export default BookDetailPage;
