import React from 'react';

import './CartPage.css';
import CartHeader from 'src/components/Header';
import CartFooter from 'src/components/Footer';
import CartContent from 'src/components/Cart/CartContent';

const CartPage = (props) => {
  if (!localStorage.getItem('cart')) {
    localStorage.setItem('cart', '[]');
  }
  return (
    <React.Fragment>
      <CartHeader />
      <CartContent />
      <CartFooter />
    </React.Fragment>
  );
};

export default CartPage;
