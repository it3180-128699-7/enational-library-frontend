import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch } from 'react-redux';
import { fetchGetBooks } from 'src/features/book/bookSlice';

import Header from 'src/components/Header';
import Footer from 'src/components/Footer';
import CategoryBanner from 'src/components/Category/CategoryBanner';
import CategoryBody from 'src/components/Category/CategoryBody';

import './Category.css';

const CategoryPage = () => {
  const dispatch = useDispatch();

  // local state
  const [filter, setFilter] = useState({
    page: 1,
    limit: 12,
    search: '',
    sort: 0,
    exclusive: 0,
    category: 0,
    author: 0,
  });

  useEffect(() => {
    dispatch(fetchGetBooks(filter));
  }, [dispatch, filter]);

  return (
    <React.Fragment>
      <Helmet>
        <title>Category | ENational Library</title>
      </Helmet>

      <div>
        <Header />
        <CategoryBanner filter={filter} setFilter={setFilter} />
        <CategoryBody filter={filter} setFilter={setFilter} />
        <Footer />
      </div>
    </React.Fragment>
  );
};

export default CategoryPage;
