import React from 'react';
import { Helmet } from 'react-helmet';

import Header from 'src/components/Header';
import Footer from 'src/components/Footer';
import HomeBanners from 'src/components/Home/HomeBanners';
import HomeContent from 'src/components/Home/HomeContent';
import HomeAuthors from 'src/components/Home/HomeAuthors';
import HomeFeedBack from 'src/components/Home/HomeFeedBack';

import './HomePage.css';

const HomePage = () => {
  return (
    <React.Fragment>
      <Helmet>
        <title>Home | ENational Library</title>
      </Helmet>
      <div id="main">
        <Header />
        <HomeBanners />
        <HomeContent />
        <HomeAuthors />
        <HomeFeedBack />
        <Footer />
      </div>
    </React.Fragment>
  );
};

export default HomePage;
