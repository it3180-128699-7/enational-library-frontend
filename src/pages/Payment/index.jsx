import React from 'react';
import { Helmet } from 'react-helmet';

import './PaymentPage.css';
import Header from 'src/components/Header';
import Footer from 'src/components/Footer';
import PaymentContent from 'src/components/Payment/PaymentContent';

const PaymentPage = (props) => {
  return (
    <React.Fragment>
      <Helmet>
        <title>Payment | ENational Library</title>
      </Helmet>
      <Header />
      <PaymentContent />
      <Footer />
    </React.Fragment>
  );
};

export default PaymentPage;
