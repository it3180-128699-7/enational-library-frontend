import React from 'react';
import { Helmet } from 'react-helmet';

import ProfileTemplate from 'src/layout/Profile/template';

import './ProfilePage.css';

const ProfilePage = () => {
  return (
    <React.Fragment>
      <Helmet>
        <title>Profile | ENational Library</title>
      </Helmet>
      <div>
        <ProfileTemplate />
      </div>
    </React.Fragment>
  );
};

export default ProfilePage;
