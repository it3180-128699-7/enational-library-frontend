export const formatCurrency = (money) =>
  money.toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD',
  });
